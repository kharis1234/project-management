$(document).ready(function() {
    var table = $('#example').DataTable({
        ordering: true,
        bLengthChange: false,
        iDisplayLength: 2,
        bFilter: false,
        pagingType: "full_numbers",
        bInfo: false,
        dom: "Bfrtip",
        buttons: [{
            extend: 'pdf',
            text: 'Exportar PDF',
            title: 'Nuse'
        }, {
            extend: 'excel',
            text: 'Exportar Excel',
            title: 'Nuse'
        }],
        language: {
            emptyTable: "<li class='text-danger' align='center'>NUSE não encontrada</li>",
            paginate: {
                previous: "<",
                next: ">",
                first: "|<",
                last: ">|"
            }
        }
    });
    addExtraButtons();
    $('#example').on("draw.dt", function(e) {

        addExtraButtons();
    })

    function addExtraButtons() {

        $(".dataTables_paginate .first").after("<a class='paginate_button quick_previous'><<</a>")
        $(".dataTables_paginate .last").before("<a class='paginate_button quick_next'>>></a>")
        var currentPage = table.page.info();
        if (currentPage.pages - 1 == currentPage.page) {
            $(".quick_next").addClass("disabled")
        } else if (currentPage.page == 0) {
            $(".quick_previous").addClass("disabled")
        }



        $(".quick_next").on("click", quickNext)
        $(".quick_previous").on("click", quickPrevious)

        function quickNext(e) {

            var pageToGoTo = (currentPage.page + 2) >= currentPage.pages ? currentPage.pages - 1 : (currentPage.page + 2);
            table.page(pageToGoTo).draw(false);
        }

        function quickPrevious(e) {

            var pageToGoTo = (currentPage.page - 2) <= 0 ? 0 : (currentPage.page - 2);
            table.page(pageToGoTo).draw(false);
        }
    }
})