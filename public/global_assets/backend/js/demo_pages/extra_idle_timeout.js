/* ------------------------------------------------------------------------------
 *
 *  # Idle timeout
 *
 *  Demo JS code for extra_idle_timeout.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var IdleTimeout = function() {


    //
    // Setup module components
    //

    // Idle timeout
    var _componentIdleTimeout = function() {

      

        if (!$.sessionTimeout) {
            console.warn('Warning - session_timeout.min.js is not loaded.');
            return;
        }

        // Idle timeout
        $.sessionTimeout({
            heading: 'h5',
            title: 'Idle Timeout',
            message: 'Sesi Anda akan berakhir. Apakah Anda ingin tetap terhubung?',
            warnAfter: 50000,
            redirAfter: 150000,
            keepAliveUrl: '/',
            redirUrl: '../admin/unlock',
            logoutUrl: '../admin/login'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentIdleTimeout();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    IdleTimeout.init();
});
