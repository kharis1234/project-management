<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
// use App\Http\Controllers\ProjectController;

class ModuleController extends Controller
{
    Private $appkey = "Idgz1PE3zO9iNc0E3oeH3CHDPX9MzZe3";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request, $id)
    {
           // if(session()->has('userid')){

        Session::put('projectid', $id);

        $projectid =  "kharis";
       

        $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module",
                "condition"=> [
                         [
                            "column" => "project_id",
                             "comparison_operator"=> "equal",
                             "value"=>$id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);

            // Get Project Name from
           $response2 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
               "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);
            $data2 = $response2->getBody();
            $data2 = json_decode($data2, true);

             foreach ($data2['data'] as $datas) {
            $projectid = $datas['id'];
            $projectname = $datas['project_name'];
             }

            $data = $response->getBody();
            $data = json_decode($data, true);


            Session::put('projectname', $projectname);


          //  dd($data);

        return view ('project_module.index', compact('data','projectname'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

          $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_module_status",
             "condition"=> "",
             "sort_by" => "description"

            ], ]);

            $data = $response->getBody();
            $modulestatus = json_decode($data, true);
            // dd($modulestatus);

          return View('project_module.create', compact('modulestatus'));
    }

    public function boot()
    {
        Route::pattern('id', '[0-9]+');

        parent::boot();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "insert_project_module",
                    "condition"=> "",
                    "value" => [
                        "module_name" => $request->modulename,
                        "description" => $request->description,
                        "project_id" => Session::get('projectid'),
                        "module_status_id" => $request->modulestatus ,
                        "target_end_date" => \Carbon\Carbon::parse($request->target_end_date)->format('Y-m-d'),
                        "target_end_date_real" => \Carbon\Carbon::parse($request->target_end_date_real)->format('Y-m-d'),]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
             // dd($data);
            // return back();

             return redirect ('/projectdetail/'. Session::get('projectid').'/#card-tab3')->with('flash_message_success2','Update a row Seccessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // dd($id);

         $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => session()->get('signature'),
             "sess_id"=> session()->get('sessid'),
             "action" => "list_project_module",
              "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);


            foreach ($data['data'] as $datas) {
            $projectmoduleid = $datas['id'];
             $projectmodulename = $datas['module_name'];
           
             }

            // dd($projectmodulename);die;


      
         $response2 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_module_status",
             "sort_by" => "description"

            ], ]);

            $data2 = $response2->getBody();
            $modulestatus = json_decode($data2, true);


            $response3 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[
            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
                "condition"=> "",
                // [
                //          [
                //             "column" => "project_module_id",
                //              "comparison_operator"=> "equal",
                //              "value"=>$id,
                //               "logical_operator"=> "AND"
                //             ],
                           
                //   ],
              "sort_by" => "project_module_id"],

            ]);


            $data3 = $response3->getBody();
            $data3 = json_decode($data3, true);

            foreach ($data3['data'] as $datas) {
            $taskid = $datas['id'];
            $taskparentid = $datas['parent_id'];
             $taskmoduleid = $datas['project_module_id'];
             }

              $response4 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user",
             "condition"=> "",]

            ]);

                 $data4 = $response4->getBody();
            $listuser = json_decode($data4, true);

           // dd($projectmoduleid);
          
    //           
         return view ('project_module.detail', compact('data','data3','id', 'projectmoduleid','modulestatus','listuser','projectmodulename'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // dd($id);

         $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => session()->get('signature'),
             "sess_id"=> session()->get('sessid'),
             "action" => "list_project_module",
              "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);


            foreach ($data['data'] as $datas) {
            $projectmoduleid = $datas['id'];
           
             }


      
         $response2 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_module_status",
             "sort_by" => "description"

            ], ]);

            $data2 = $response2->getBody();
            $modulestatus = json_decode($data2, true);


            $response3 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[
            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
                "condition"=> "",
                // [
                //          [
                //             "column" => "project_module_id",
                //              "comparison_operator"=> "equal",
                //              "value"=>$id,
                //               "logical_operator"=> "AND"
                //             ],
                           
                //   ],
              "sort_by" => "project_module_id"],

            ]);


            $data3 = $response3->getBody();
            $data3 = json_decode($data3, true);

            foreach ($data3['data'] as $datas) {
            $taskid = $datas['id'];
            $taskparentid = $datas['parent_id'];
             $taskmoduleid = $datas['project_module_id'];
             }

              $response4 = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user",
             "condition"=> "",]

            ]);

                 $data4 = $response4->getBody();
            $listuser = json_decode($data4, true);

           // dd($projectmoduleid);
          
    //           
         return view ('project_module.edit', compact('data','data3','id', 'projectmoduleid','modulestatus','listuser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {


           $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                         "module_name"=> $request->modulename,
                        "description" => $request->description,
                        "project_id" => $request->projectid,
                        "module_status_id" => $request->modulestatus,
                        // "is_active"=> 1,
                        "target_end_date"=> \Carbon\Carbon::parse($request->target_end_date)->format('Y-m-d'),
                        "target_end_date_real"=> \Carbon\Carbon::parse($request->target_end_date_real)->format('Y-m-d'),]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

                
    
              
          return redirect ('/projectmodule/'. $id .'/edit')->with('flash_message_success2','Update a row Seccessfully');

        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request, $id)
    {
               $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST','http://localhost:8080/test-gitlab-api/basic/web/api/service',[

            'json' => [
             "application_key" =>$this->appkey,
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],       
                  ],
                    "value" => [
                        "is_delete" => 1,]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
            // dd($data);
            return redirect('/projectdetail/'. Session::get('projectid'))->with('flash_message_success2','Deleted a row has been Seccessfully');
    }


}
