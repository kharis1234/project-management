<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;

class TaskController extends Controller
{
     Private $appkey = "Idgz1PE3zO9iNc0E3oeH3CHDPX9MzZe3";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        // $url = 'http://localhost:8080/test-gitlab-api/basic/web/api/service';
           // if(session()->has('userid')){

       // Session::put('projectid', $id);

      $client = new Client();
        //  Get Project List
    
        $response1 = $client->request('POST', $request->session()->get('urlservice') ,[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data1 = $response1->getBody();
            $projects = json_decode($data1, true);

        //  Get Project Modules List

        $response2 = $client->request('POST', $request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data2 = $response2->getBody();
            $modules = json_decode($data2, true);

          //  Get Task List

          $response3 = $client->request('POST', $request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
              "condition"=> ""],

            ]);

            $data3 = $response3->getBody();
            $tasks = json_decode($data3, true);




             $response4 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_task_status",
             "condition"=> "",
             "sort_by" => "id"

            ], ]);

            $data4 = $response4->getBody();
            $taskstatus = json_decode($data4, true);






            // Session::put('modulename', $modulename);
        //dd($tasks['data']);die;

        return view ('tasks.index', compact('projects','modules','taskstatus','tasks'));
    }


     public function list(Request $request)
    {
      

        $client = new Client();
        //  Get Project List
    
        $response1 = $client->request('POST', $request->session()->get('urlservice') ,[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data1 = $response1->getBody();
            $projects = json_decode($data1, true);

        //  Get Project Modules List

        $response2 = $client->request('POST', $request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data2 = $response2->getBody();
            $modules = json_decode($data2, true);

          //  Get Task List

          $response3 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
              "condition"=> ""],

            ]);

            $data3 = $response3->getBody();
            $tasks = json_decode($data3, true);




             $response4 = $client->request('POST', $request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_task_status",
             "condition"=> "",
             "sort_by" => "id"

            ], ]);

            $data4 = $response4->getBody();
            $taskstatus = json_decode($data4, true);


             $response5 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user",
             "condition"=> "",]

            ]);


            $data5 = $response5->getBody();
            $listusers = json_decode($data5, true);






            // Session::put('modulename', $modulename);
        //dd($tasks['data']);die;

        return view ('tasks.list', compact('projects','modules','taskstatus','tasks','listusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          // dd($request->parenttasksid);die;


          $client = new Client();

         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "insert_project_module_task",
                    "condition"=> "",
                    "value" => [
                        "project_module_id" => $request->projectmoduleid,
                        "parent_id" => $request->parenttasksid,
                        "title" => $request->title,
                        "description" => $request->desc,
                        "developer_id" => $request->developerassignedby,
                        "developer_assigned_by" => $request->session()->get('userid') ,
                        "task_status_id" => 3,
                          "kickoff_date" => \Carbon\Carbon::parse($request->kickoff_date)->format('Y-m-d'),
                        "target_end_date" => \Carbon\Carbon::parse($request->target_end_date)->format('Y-m-d'),
                        "target_end_date_dev" => \Carbon\Carbon::parse($request->target_end_date_dev)->format('Y-m-d'),
                          "target_end_date_qa" => \Carbon\Carbon::parse($request->target_end_date_qa)->format('Y-m-d'),
                        "target_hours_dev" => $request->target_hours_dev,
                        "target_hours_qa" => $request->target_hours_qa,]

            ], ]);
             $data = $response->getBody();
            $data = json_decode($data, true);

           
            // dd($data);die;

           // Get Project Module ID     
           $projectmoduleid = $request->projectmoduleid;

          // dd($projectmoduleid);die;

         return redirect ('/projectdetail/'. $projectmoduleid)->with('flash_message_success2','Add a new task Seccessfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       
         $client = new Client();

         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                        "project_module_id" => $request->projectmoduleid,
                        "parent_id" => $request->parenttasksid,
                        "title" => $request->title,
                        "description" => $request->desc,
                        "developer_id" => $request->developerid,
                         "tester_id" => $request->testerid,
                        "developer_assigned_by" => $request->session()->get('userid') ,
                        "tester_assigned_by" => $request->session()->get('userid') ,
                        // "task_status_id" => $request->taskstatusid,
                        "kickoff_date" => \Carbon\Carbon::parse($request->kickoff_date)->format('Y-m-d'),
                        "target_end_date_dev" => \Carbon\Carbon::parse($request->target_end_date_dev)->format('Y-m-d'),
                        "target_end_date_dev" => \Carbon\Carbon::parse($request->target_end_date_dev)->format('Y-m-d'),
                        "target_end_date_qa" => \Carbon\Carbon::parse($request->target_end_date_qa)->format('Y-m-d'),
                        "target_hours_dev" => $request->target_hours_dev,
                        "target_hours_qa" => $request->target_hours_qa,]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

          //  dd($request->testerid);die;

        $projectmoduleid = $request->projectmoduleid;
          
         return redirect ('/projectdetail/'. $projectmoduleid)->with('flash_message_success2','Update a row Seccessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request, $id)
    {

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "deactivate_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
                    "value" => [
                        // "project_module_id" =>$request->projectmoduleid ,
                        "is_active" => 0,
                        "is_delete" => 1,]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

            $response2 = $client->request('POST',$request->session()->get('urlservice'),[
            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
                "condition"=> "",
                [
                         [
                            "column" => "project_module_id",
                             "comparison_operator"=> "equal",
                             "value"=>$id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "project_module_id"],

            ]);

      

             $data2 = $response2->getBody();
            $data2 = json_decode($data2, true);

              foreach ($data2['data'] as $datas) {
            $projectmoduleid = $datas['project_module_id'];
           
         }

          // dd($projectmoduleid); 


            
            
       
          return redirect ('/projectdetail/'. $projectmoduleid)->with('flash_message_success2','Delete a row Seccessfully');
    }

    public function taketask(Request $request, $id)
    {

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                        "kickoff_date_dev" => \Carbon\Carbon::parse($request->kickoff_date_dev)->format('Y-m-d'),
                        "task_status_id" => 2,
                      ]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

           
            //dd($data);die;

               // $projectmoduleid = $request->projectmoduleid;
          
         return redirect ('/tasks')->with('flash_message_success2','Submit Task has Seccessfully');
    }

     public function taskdone(Request $request, $id)
    {
         $startdate = strtotime($request->kickoff_date_dev);
         $enddevreal = strtotime($request->target_end_date_dev_real);
         $diffdate = $this->networkdays($startdate, $enddevreal);

        // dd($br_dana);die();
         $target_hours_dev_real = $diffdate * 8 ;

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                        "target_end_date_dev_real" => \Carbon\Carbon::parse($request->target_end_date_dev_real)->format('Y-m-d'),
                        "target_hours_dev_real" => $target_hours_dev_real,
                        "task_status_id" => 3,
                      ]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

           
            //dd($data);die;

               // $projectmoduleid = $request->projectmoduleid;
          
         return redirect ('/tasks')->with('flash_message_success2','Submit Task has Seccessfully');
    }


    public function testingtask(Request $request, $id)
    {

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                        "kickoff_date_qa" => \Carbon\Carbon::parse($request->kickoff_date_qa)->format('Y-m-d'),
                        "task_status_id" => 4,
                      ]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

           
            //dd($data);die;

               // $projectmoduleid = $request->projectmoduleid;
          
         return redirect ('/tasks')->with('flash_message_success2','Testing Task');
    }

     public function testingdone(Request $request, $id)
    { 
         $startdate = strtotime($request->kickoff_date_qa);
         $endqareal = strtotime($request->target_end_date_qa_real);
         $diffdate = $this->networkdays($startdate, $endqareal);
         $target_hours_qa_real = $diffdate * 8 ;
        // dd($target_hours_qa_real);die();

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project_module_task",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],  
                  ],
                    "value" => [
                        "target_end_date_qa_real" => \Carbon\Carbon::parse($request->target_end_date_qa_real)->format('Y-m-d'),
                        "target_hours_qa_real" => $target_hours_qa_real,
                        "task_status_id" => 5,
                      ]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

           
          //  dd($data);die;

               // $projectmoduleid = $request->projectmoduleid;
          
         return redirect ('/tasks')->with('flash_message_success2','Test Done');
    }


  public  function networkdays($s, $e, $libur = array())
{
    if ($s > $e) {
        return $this->networkdays($e, $s, $libur);
    }
    $sd = date("N", $s);
    $ed = date("N", $e);
    $w = floor(($e - $s) / (86400 * 7));
  
    if ($ed >= $sd) {
        $w--;
    }
    $nwd = max(6 - $sd, 0);
    $nwd += min($ed, 5);
     $nwd += $w * 5;
      foreach ($holidays as $h) {
        $h = strtotime($h);
        if ($h > $s && $h < $e && date("N", $h) < 6) {
            $nwd--;
        }
    }
    return $nwd;
}



}
