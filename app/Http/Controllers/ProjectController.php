<?php

namespace App\Http\Controllers;

// use App\project;
use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;

class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 


         // if(session()->has('userid')){

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

               // dd($data['data'][0]['project_name']);
           
          //  dd($data);

        return view ('project.index', compact('data'));
            // return view('project.project');


            // }
            //   Alert::error('Sesi Telah berakhir, Solihakan Login kembali', 'Session Expire');
            //      return view('admin.admin_login')->with('flash_message_error2','Session Expire, Please Login Again');

        }

    // Fungsi untuk Cek ID bahwa ID adalah Numeric
    public function boot()
    {
    Route::pattern('id', '[0-9]+');
    parent::boot();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     { 

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "insert_project",
                    "condition"=> "",
                    "value" => [
                        "project_name" => $request->projectname,
                        "kickoff_date" => \Carbon\Carbon::parse($request->kickoff_date)->format('Y-m-d'),
                        "target_end_date" => \Carbon\Carbon::parse($request->target_end_date)->format('Y-m-d'),]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
            // dd($data);
            return redirect ('/project')->with('flash_message_success2','Insert a row Seccessfully');
        }
        

    /**
     * Display the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => session()->get('signature'),
             "sess_id"=> session()->get('sessid'),
             "action" => "list_project",
              "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
          


        
    //           
         return view ('project.edit', compact('data','id'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                          
                  ],
                    "value" => [
                        "project_name" => $request->project_name,
                        "kickoff_date" => \Carbon\Carbon::parse($request->kickoff_date)->format('Y-m-d'),
                        "target_end_date" => \Carbon\Carbon::parse($request->target_end_date)->format('Y-m-d'),]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
            //  dd($data);
            return redirect ('/project')->with('flash_message_success2','Update a row Seccessfully');

        }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
                    "value" => [
                        "is_delete" => 1,]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);
            // dd($data);
       
        return redirect('/project')->with('flash_message_success2','Deleted a row has been Seccessfully');
    }

    Public function getProjectById(Request $request, $id){

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
                "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

        foreach ($data['data'] as $datas) {
            $projectid = $datas['id'];
            $projectname = $datas['project_name'];
         }

       //  dd($projectid , $projectname );
        return  $projectname;

    }


    // Add Members


    Public function addmembers(Request $request, $id){

            $client = new Client();
            $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "edit" => true,
             "action" => "update_project",
                      "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                          
                  ],
                    "value" => [
                        "members" => implode(',', $request->members),
                        ]

            ], ]);
            $data = $response->getBody();
            $data = json_decode($data, true);


             return redirect ('/projectdetail/'. Session::get('projectid').'/#card-tab2')->with('flash_message_success2','Add members has been Successfully');


    }

    public function getProjects(Request $request)
    {

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
             "condition"=> "",
              "sort_by" => "created_date"],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

            return $data;

    }



}
