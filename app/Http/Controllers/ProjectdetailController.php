<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;

class ProjectdetailController extends Controller
{

     Private $appkey = "Idgz1PE3zO9iNc0E3oeH3CHDPX9MzZe3";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request, $id)
    {
       // if(session()->has('userid')){

        Session::put('projectid', $id);



        // $projectid =  "kharis";
       

         $client = new Client();
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module",
                "condition"=> [
                         [
                            "column" => "project_id",
                             "comparison_operator"=> "equal",
                             "value"=>$id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);

            // Get Project Name from
           $response2 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
               "condition"=> [
                         [
                            "column" => "id",
                             "comparison_operator"=> "equal",
                             "value"=> $id,
                              "logical_operator"=> "AND"
                            ],
                           
                  ],
              "sort_by" => "created_date"],

            ]);

             $response3 = $client->request('POST',$request->session()->get('urlservice'),[
            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
                "condition"=> "",
                // [
                //          [
                //             "column" => "project_module_id",
                //              "comparison_operator"=> "equal",
                //              "value"=>$id,
                //               "logical_operator"=> "AND"
                //             ],
                           
                //   ],
              "sort_by" => "project_module_id"],

            ]);


          $response4 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user",
             "condition"=> "",]

            ]);

          $response5 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_role",
             "condition"=> "",]

            ]);

           $response6 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "condition"=> "",
             "edit" => false,
             "action" => "list_project_module_status",
             "condition"=> "",
             "sort_by" => "description"

            ], ]);

            $response7 = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user_role",
             "condition"=> "",]

            ]);

            $data6 = $response6->getBody();
            $modulestatus = json_decode($data6, true);


            $data4 = $response4->getBody();
            $listuser = json_decode($data4, true);



            $data5 = $response5->getBody();
            $listrole = json_decode($data5, true);



            $data2 = $response2->getBody();
            $data2 = json_decode($data2, true);

            $data7 = $response7->getBody();
            $userroles = json_decode($data7, true);  

              // dd($data2);die();

            foreach ($data2['data'] as $datas) {
            $projectid = $datas['id'];
            $members = $datas['members'] ;
            $projectname = $datas['project_name'];
             }

              $membersexplode =  explode(',', $datas['members']) ;
              $membersexplode3 = 3 ;

            //  dd($membersexplode);die;

            // foreach ($membersexplode as $key=>$value) {
          
            //     dd($key);die();

            //  }

             

              // dd($membersexplode);die();



                $data3 = $response3->getBody();
            $tasks = json_decode($data3, true); 

            //dd($tasks);die;

            foreach ($tasks['data'] as $datas) {
          
            $projectmoduleid = $datas['project_module_id'];
             }

          

            $data = $response->getBody();
            $projects = json_decode($data,true);

            //  foreach ($data['data'] as $datas) {
            // $moduleid = $datas['id'];
            // $modulename = $datas['module_name'];
            //  }


             // $counttask = COUNT( $moduleid == $taskmoduleid;


            // Session::put('projectmoduleid', $projectmoduleid);


          //  dd($data);

        return view ('project.detail', compact('projects','tasks','data2','projectname','moduleid','modulename','taskmoduleid','listuser','listrole','taskid','taskparentid','projectmoduleid', 'projectid','modulestatus','members','membersexplode','userroles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function getUser()
    {



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
