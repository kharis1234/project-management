<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Auth;
use Session;
// use App\User;
// use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
//use Alert;


class AdminController extends Controller
{
    

    public function index(){

        return view ('admin.admin_login');
    }


    public function login(Request $request){

      if($request->isMethod('post')){
        $data = $request->input();
        if(Auth::attempt(['email'=>$data['email'], 'password'=>$data['password'], 'admin'=>'1'])){
          // return "success"; die;

           // if using Session to Protect Admin Page
          // session()->put('adminSession', $data['email']);
          return redirect ('/admin/dashboard')->with('flash_message_success2','logged in Seccessfully');

      }else{

      return redirect ('/admin')->with('flash_message_error','Invalid login, Your email & password incorrect');
      }
    
    }

   }

     

// Proses Login Authentication 
     public function prosesLogin(Request $request){
    
        $email = $request->email;
        $password = $request->password;
        $urllogin = 'http://localhost:8080/test-gitlab-api/basic/web/api/auth/login';
        Session::put('appkey', 'Idgz1PE3zO9iNc0E3oeH3CHDPX9MzZe3');

        if( isset( $email) && isset( $password))
        {
            
        Session::put('email',$email);
        Session::put('password', $password);

        $client = new Client();
        $response = $client->request('POST', $urllogin ,[

            'json' => [
             "email" =>  $email,
             "password" => $password,
             "application_key"=>$request->session()->get('appkey')],
            ]);

            $data = $response->getBody();
            $data = json_decode($data);
            // dd($data);
            
                if( isset( $data->user_id ) )
                    {
                     $userid =$data->user_id;
                     $gitlabusername = $data->gitlab_username;
                     $employeename = $data->employee_name;
                     $roleid = $data->role_id;
                     $sessid =$data->sess_id;
                     $signature =$data->signature;
                     Session::put('userid', $userid);
                     Session::put('gitlabusername', $gitlabusername);
                     Session::put('roleid', $roleid);
                     Session::put('employeename', $employeename);
                     Session::put('sessid', $sessid);
                     Session::put('signature', $signature);

                     // App key
                     

                     // URL Service
                     Session::put('urlservice', 'http://localhost:8080/test-gitlab-api/basic/web/api/service');

                    
                    return redirect ('/admin/dashboard')->with('flash_message_success2','logged in Seccessfully');
                    }
                else{
                    return redirect ('/admin/login')->with('flash_message_error','Invalid login, Your Email & Password Incorrect');
                    }
        }else{
            return redirect ('/admin/login')->with('flash_message_error','Invalid login, Please Insert Your Email & Password Correctly');
             }

    }
  

    public function dashboard(Request $request){

    
            $client = new Client();

            $response = $client->request('POST',$request->session()->get('urlservice'),[
            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project",
              "condition"=> "",
              ],

            ]);

             $response2 = $client->request('POST',$request->session()->get('urlservice'),[
            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
               "condition"=> [
                         [
                            "column" => "task_status_id",
                             "comparison_operator"=> "equal",
                             "value"=> 1,
                              "logical_operator"=> "OR"
                            ],

                             [
                            "column" => "task_status_id",
                             "comparison_operator"=> "equal",
                             "value"=> 2,
                              "logical_operator"=> "OR"
                            ], 
                            [
                            "column" => "task_status_id",
                             "comparison_operator"=> "equal",
                             "value"=> 3,
                              "logical_operator"=> "OR"
                            ],

                               [
                            "column" => "task_status_id",
                             "comparison_operator"=> "equal",
                             "value"=> 4,
                              "logical_operator"=> "OR"
                            ],         
                  ],
              ],

            ]);

               $response3 = $client->request('POST',$request->session()->get('urlservice'),[
            'json' => [
             "application_key" =>$request->session()->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_project_module_task",
               "condition"=> [
                         [
                            "column" => "task_status_id",
                             "comparison_operator"=> "equal",
                             "value"=> 5,
                              "logical_operator"=> "AND"
                            ],

                        ],
              ],

            ]);

            $dataprojects = $response->getBody();
            $dataprojecttask = $response2->getBody();
            $dataprojecttaskcompleted = $response3->getBody();

            $projects = json_decode($dataprojects, true);
            $projectstasks = json_decode($dataprojecttask, true);
             $projecttaskcompleted = json_decode($dataprojecttaskcompleted, true);

            // foreach($projectstasks['data'] as $projectstask)
            // {
            //   dd($projectstask['id']);
            // }


          //  dd(count($projectstasks['data']));die();

            //dd(count($projects_task['data']));die();
          return view ('admin.dashboard', compact('projects','projectstasks','projecttaskcompleted'));
    }

    public function settings(){
      return view('admin.settings');
    }

    public function chkPassword(Request $request){

      $data = $request->all();
      $current_password = $data['currentpassword'];
      $check_password = User::where(['admin'=>'1'])->first();
      if(Hash::check($current_password, $check_password->password)){
        echo "true"; die;
      }else{
        echo "false"; die;
      }

    }

    public function updatePassword(Request $request){

      if($request->isMethod('post')){
        $data = $request->all();
        $check_password = User::where(['email'=> Auth::user()->email])->first();
        $current_password = $data['currentpassword'];
        if(Hash::check($current_password, $check_password->password)){

          $password = bcrypt($data['password']);
          User::where('id','1')->update(['password'=> $password]);
          return redirect ('/admin/settings')->with('flash_message_success','Password update Successfully');
        }else{
          return redirect ('/admin/settings')->with('flash_message_error','Incorrect Current Password');
        }
      }
    }

    public function logout(){
      session()->flush();
      return redirect ('/admin/login')->with('flash_message_success','logged out Seccessfully');

    }

     public function unlock(){
   
        return view ('admin.unlock');
    }


    public function prosesUnlock(Request $request){


        $password = $request->password;

        if($password == Session::get('password'))
        {

          return redirect ('/admin/dashboard')->with('flash_message_success2','logged in Seccessfully');

        }else{

             return redirect ('/admin/unlock')->with('flash_message_error','Invalid login, Please Insert Your Password Correctly');
        }
            
    }

}
