<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;

class UserController extends Controller
{

    Private $appkey = "Idgz1PE3zO9iNc0E3oeH3CHDPX9MzZe3";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function getUser(Request $request){

         $client = new Client([

        'header' => ['content-type'=> 'application/json', 'Accept' => 'application/json'],'cookies' => true]);
         $response = $client->request('POST',$request->session()->get('urlservice'),[

            'json' => [
             "application_key" =>$request->get('appkey'),
             "signature" => $request->session()->get('signature'),
             "sess_id"=> $request->session()->get('sessid'),
             "action" => "list_user",
             "condition"=> ""],

            ]);
            $data = $response->getBody();
            $data = json_decode($data, true);

            dd($data);

        return view ('user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $company = Company::where('id' , $id->id)

        // return view('content.show', ['company' => $company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
