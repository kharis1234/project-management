<?php

namespace App\Http\Middleware;

use Closure;

class Authsession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
          if(!$request->session()->exists('sessid') ){

            return redirect ('/')->with('flash_message_error','Restricted Page Area, Please login');
        }

       // return view ('admin.dashboard');
        return $next($request);
    }
}
