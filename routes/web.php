<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.admin_login');
});
	Route::get('admin/login', 'AdminController@index' );
	Route::get('admin/logout', 'AdminController@logout' );
	Route::match(['get', 'post'], '/prosesUnlock', 'AdminController@prosesUnlock' );
	Route::match(['get', 'post'], '/prosesLogin', 'AdminController@prosesLogin' );
	Route::match(['get', 'post'], '/proseseEdit', 'AdminController@prosesEdit' );
	Route::get('admin/unlock', 'AdminController@unlock' );
	Route::get('user/list', 'UserController@getUser' );

// Route Group with Middleware Session	

// Route::middleware('authsession')->group(function () {    // Alternatif
Route::group(['middleware' => 'authsession'], function() {
	Route::get('/dasboard', 'DashboardController@index');

	// Admin
	Route::get('/admin/dashboard', 'AdminController@dashboard');
	Route::get('/admin', 'AdminController@dashboard' );
	Route::get('/admin/settings', 'AdminController@settings' );
	Route::get('admin/check-pwd', 'AdminController@chkPassword' );
	Route::match(['get', 'post'], '/admin/update-pwd', 'AdminController@updatePassword' );
	Route::get('admin/unlock', 'AdminController@unlock' );

	// Project
	Route::get('/project', 'ProjectController@index')->name('project');
	Route::get('/project/create', 'ProjectController@create');
	Route::post('/project/{id}/addmembers', 'ProjectController@addmembers');
	Route::post('/project/store','ProjectController@store');
	Route::get('/project/{id}/edit','ProjectController@edit');
	Route::post('/project/{id}/edit','ProjectController@update');
	Route::get('/project/{id}/get','ProjectController@getProjectById');
	Route::get('/project/{id}/delete','ProjectController@destroy');

	//

	Route::get('/projectdetail/{id}', 'ProjectdetailController@index');
	
	// Module
	Route::get('/projectmodule', 'ModuleController@index')->name('projectmodule');
	Route::get('/projectmodule/{id}/create', 'ModuleController@create');
	Route::post('/projectmodule/store','ModuleController@store');
	Route::get('/projectmodule/{id}','ModuleController@index');
	Route::get('/projectmodule/{id}/edit','ModuleController@edit');
	Route::get('/projectmodule/{id}/view','ModuleController@show');
	Route::post('/projectmodule/{id}/edit','ModuleController@update');
	Route::get('/projectmodule/{id}/delete','ModuleController@destroy');

	// Module
	Route::get('/tasks', 'TaskController@list');
	Route::get('/tasks/{id}/create', 'TaskController@create');
	Route::post('/tasks/store','TaskController@store');
	// Route::get('/taskslist/{id}','TaskController@tasklist');
	// Route::get('/tasks','TaskController@index');
	Route::get('/tasks/{id}/edit','TaskController@edit');
	Route::post('/tasks/{id}/edit','TaskController@update');
	Route::get('/tasks/{id}/delete','TaskController@destroy');
	Route::post('/tasks/{id}/taketask','TaskController@taketask');
	Route::post('/tasks/{id}/taskdone','TaskController@taskdone');
	Route::post('/tasks/{id}/testingtask','TaskController@testingtask');
	Route::post('/tasks/{id}/testingdone','TaskController@testingdone');
	// Route::post('/tasks/{id}/changetaskstatus','TaskController@changetaskstatus');


	// Route::redirect('/here', '/there', 404);
	// Route::view('/admin/welcome', '/project/create');
	// Route::get('/project/edit', 'ProjectController@edit');
	// Route::post('/project/update/{id}', array('as' => 'project.update', 'uses' => 'ProjectController@update'));
	// Route::get('/project/destroy/{id}','ProjectController@destroy');
	// Route::resource('project', 'ProjectController');
   
});

// Route::get('/admin', 'AdminController@login' );



// Route::group(['middleware' => 'auth'], function() {
//     Route::get('/admin/dashboard', 'AdminController@dashboard' );
//     Route::get('/admin/settings', 'AdminController@settings' );
//     Route::get('admin/check-pwd', 'AdminController@chkPassword' );
//     Route::match(['get', 'post'], '/admin/update-pwd', 'AdminController@updatePassword' );
//     // Route::get('admin/update-pwd', 'AdminController@updatePassword' );
// });
	
    
   
    
    // Route::get('admin/update-pwd', 'AdminController@updatePassword' );
  
	// Route::post('admin','GuzzleController@index' );
	
	// Auth::routes();
	// Route::get('/home', 'HomeController@index')->name('home');




