	@extends ('layouts.adminLayout.admin_design')
	@section('js_picker_date')
	<!-- Theme JS files -->

	   <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/legacy.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>
     <script src="{{ asset('global_assets/backend/js/demo_pages/form_checkboxes_radios.js') }}"></script>
   

	@endsection
	@section ('content')

<div class="content">

				<!-- Form validation -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create Project</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
								@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
								@endif

					
								@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
								@endif

						<form class="form-validate-jquery" action="{{action('ProjectController@update', $id)}}" method="post"> 
							{{ csrf_field() }}

							@foreach($data['data'] as $dataproject)
							<fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Form Field</legend>
								
								<!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Project_name <span class="text-danger">*</span></label>
									<div class="col-lg-4 input-group">
										<input type="text" name="project_name" id="project_name" class="form-control" required placeholder="Please insert project" value="{{$dataproject['project_name']}}">
									</div>
								</div>

								
								{{-- <div class="form-group row">
									<label class="col-form-label col-lg-3">Date <span class="text-danger">*</span></label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="created_date" id="created_date" class="form-control daterange-single" required placeholder="Please insert project" value="value="{{ $project->name }}"">
									</div>
								</div> --}}
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Kick Off Date  <span class="text-danger">*</span></label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single" required placeholder="Please insert project" value="{{ \Carbon\Carbon::parse($dataproject['kickoff_date'])->format('m/d/Y') }}">

									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Target End Date  <span class="text-danger">*</span></label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single" required placeholder="Please insert project" value="{{ \Carbon\Carbon::parse($dataproject['target_end_date'])->format('m/d/Y') }}">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Creator <span class="text-danger">*</span></label>
									<div class="col-lg-3 input-group">
										<input type="text" id="creator" name="creator" readonly value="{{Session::get('employeename')}}" id="project_name" class="form-control" required placeholder="Please insert project">
									</div>
								</div>

								<div class="form-check form-check-switchery">
									<label class="form-check-label">
										
										<input type="checkbox" class="form-check-input-switchery-primary" checked data-fouc>
										 <span class="font-weight-semibold">Status</span> 
									</label>
								</div>

							

							</fieldset>
							@endforeach

							<div class="d-flex justify-content-end align-items-center">
								<button type="reset" class="btn btn-light" id="reset">Reset <i class="icon-reload-alt ml-2"></i></button>
								<button type="submit" class="btn btn-primary ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
				<!-- /form validation -->

			</div>

	@endsection