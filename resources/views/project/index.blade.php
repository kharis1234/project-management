@extends ('layouts.adminLayout.admin_design')
	
	@section('js_datatables')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/contextmenu.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/table_elements.js') }}"></script>
   

    @endsection

    @section('js_dashboard')
		<script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.ui.min.js') }}"></script>
	<script src="{{ asset('global_assets/backend/js/demo_pages/animations_velocity_examples.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/session_timeout.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_idle_timeout.js') }}"></script>
	@endsection

	@section ('content')
  

	<div class="content">
		<!-- Data tables Responsive -->
				<div class="card">
					<div class="card-header bg-info text-white header-elements-inline">
						<h5 class="card-title">Project Page</h5>
						<div class="header-elements">
							<div class="list-icons">
								@if(Session::get('roleid') == 1 )
								<div style="margin-right:10px;"><a href="{{action('ProjectController@create', Session::get('projectid') )}}"><button type="button" class="btn alpha-blue text-blue-800 border-blue-600">
					                			
				                			Add a project <i class="icon-file-plus  "></i> </button></a>&nbsp;&nbsp;&nbsp; |</div> 
				                @endif
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

				{{-- 	<div class="card-body">
						Project List
					</div> --}}
					<ul style="margin-left: -40px;">

					<table class="table datatable-responsive-row-control table-hover datatable-highlight">
						<thead>
							<tr>
								<th></th>
								<th>Project Name</th>
								{{-- <th>Module</th> --}}
								<th>Date</th>
								<th>Creator</th>
								<th>Kick Off Date</th>
								<th>Tagret End Date</th>
								<th>Status</th>
								@if(Session::get('roleid') == 1 )
								<th class="text-center">Actions</th>
								@endif
							</tr>
						</thead>
						<tbody>
							
							@foreach($data['data'] as $dataproject)
							@if($dataproject['is_delete'] == 0  )
							<tr>
									<td></td>
								<td><a href="{{action('ProjectdetailController@index', $dataproject['id'] )}}" class="list-icons-item" style="font-color:blue; font-weight: 600;" data-popup="tooltip" title="Module" data-container="body">{{ $dataproject['project_name'] }} </a></td>
								

								<td>{{ \Carbon\Carbon::parse($dataproject['created_date'])->format('d/m/Y')}}</td>
								<td>{{ $dataproject['creator'] }}</td>
								<td>{{ \Carbon\Carbon::parse($dataproject['kickoff_date'])->format('d/m/Y')}}</td>
								<td>{{ \Carbon\Carbon::parse($dataproject['target_end_date'])->format('d/m/Y')}}</td>
								
								<td>
								@if( $dataproject['is_active'] == 0)
									<span class="badge badge-secondary">Inctive</span>
								@elseif($dataproject['is_active'] == 1)
									<span class="badge badge-success">Active</span>
								@else
									<span class="badge badge-info">Pending</span>
								@endif
							</td>
							@if(Session::get('roleid') == 1 )
								<td class="text-center">
									
								<div class="list-icons">
					                		<a href="{{action('ProjectController@edit', $dataproject['id'])}}" class="list-icons-item" data-popup="tooltip" title="Edit" data-container="body">
					                			<i class="icon-pencil7"></i>
				                			</a>
					                		
					                		{{-- <a href="" class="list-icons-item" data-popup="tooltip" title="View" data-container="body">
					                			<i class="icon-search4 "></i>
				                			</a> --}}
				                			
				                			<a href="{{action('ProjectController@destroy', $dataproject['id'])}}" class="list-icons-item" data-popup="tooltip" title="Remove" data-container="body">
					                			<i class="icon-trash"></i>
				                			</a> 
				                			
					                	</div>
					                	
								</td>
							@endif
   
							   </tr>

							   @endif
							@endforeach
						
							
						</tbody>
					</table>
				</div>
				<!-- /Data tables Responsive -->

			</div>

		@endsection