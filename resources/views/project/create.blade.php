	@extends ('layouts.adminLayout.admin_design')
	@section('js_picker_date')
	<!-- Theme JS files -->

	   <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/legacy.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>

	@endsection
	@section ('content')

<div class="content">

				<!-- Form validation -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create Project</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
								@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
								@endif

					
								@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
								@endif

						<form class="form-validate-jquery" action="{{ url('/project/store') }}" method="post"> {{ csrf_field() }}
							<fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Form Field</legend>
								
								<!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Project_name <span class="text-danger">*</span></label>
									<div class="col-lg-4 input-group">
										<input type="text" name="projectname" id="projectname" class="form-control" required placeholder="Please insert project">
									</div>
								</div>

								
							{{-- 	<div class="form-group row">
									<label class="col-form-label col-lg-3">Date <span class="text-danger">*</span></label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="created_date" id="created_date" class="form-control daterange-single" required placeholder="Please insert project">
									</div>
								</div> --}}
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Start Date  </label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single"  placeholder="Please insert kickoff date">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Deadline  </label>
									<div class="col-lg-2 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single"  placeholder="Please insert target end date">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Creator <span class="text-danger">*</span></label>
									<div class="col-lg-3 input-group">
										<input type="text" name="creator" id="creator"readonly value="{{Session::get('employeename')}}" id="project_name" class="form-control" >
									</div>
								</div>

							

							</fieldset>

							<div class="d-flex justify-content-end align-items-center">
								<button type="reset" class="btn btn-light" id="reset">Reset <i class="icon-reload-alt ml-2"></i></button>
								<button type="submit" class="btn btn-primary ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
				<!-- /form validation -->

			</div>

	@endsection