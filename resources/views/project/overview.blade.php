	<div class="row">
					<div class="col-sm-6 col-xl-3">
						<div class="card card-body">
							<div class="media">
								<div class="mr-3 align-self-center">
									<i class="icon-pointer icon-3x text-success-400"></i>
								</div>

								<div class="media-body text-right">
									<h3 class="font-weight-semibold mb-0">10/40</h3>
									<span class="text-uppercase font-size-sm text-muted">Submited/Requested</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-xl-3">
						<div class="card card-body">
							<div class="media">
								<div class="mr-3 align-self-center">
									<i class="icon-enter6 icon-3x text-indigo-400"></i>
								</div>

								<div class="media-body text-right">
									<h3 class="font-weight-semibold mb-0">20</h3>
									<span class="text-uppercase font-size-sm text-muted">In Progress</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-xl-3">
						<div class="card card-body">
							<div class="media">
								<div class="media-body">
									<h3 class="font-weight-semibold mb-0">10</h3>
									<span class="text-uppercase font-size-sm text-muted">Ready to Test</span>
								</div>

								<div class="ml-3 align-self-center">
									<i class="icon-bubbles4 icon-3x text-blue-400"></i>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-xl-3">
						<div class="card card-body">
							<div class="media">
								<div class="media-body">
									<h3 class="font-weight-semibold mb-0">20/40</h3>
									<span class="text-uppercase font-size-sm text-muted">Completed Tasks</span>
								</div>

								<div class="ml-3 align-self-center">
									<i class="icon-bag icon-3x text-danger-400"></i>
								</div>
							</div>
						</div>
					</div>
				</div>