@extends ('layouts.adminLayout.admin_design')
	
	@section('js_datatables')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/contextmenu.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/table_elements.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_trees.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_basic.js') }}"></script>
   
     <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/form_inputs.js') }}"></script>
        <script src="{{ asset('global_assets/backend/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/select2.min.js') }}"></script>
   <script src="{{ asset('global_assets/backend/js/demo_pages/components_modals.js') }}"></script>  

   	   <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/legacy.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>
     <script src="{{ asset('global_assets/backend/js/demo_pages/form_checkboxes_radios.js') }}"></script>

      <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/select2.min.js') }}"></script>
     <script src="{{ asset('global_assets/backend/js/demo_pages/form_select2.js') }}"></script> 

    @endsection


    @section('js_dashboard')
	{{-- 	<script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.ui.min.js') }}"></script>
	<script src="{{ asset('global_assets/backend/js/demo_pages/animations_velocity_examples.js') }}"></script> --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/session_timeout.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_idle_timeout.js') }}"></script>
	@endsection

	@section ('content')


	<div class="content">
		<!-- Data tables Responsive -->
				<div class="card">

					<div class="card-header alpha-success text-success-800 header-elements-inline">
						 <h5 class="card-title"><button type="button" class="btn badge-striped" data-popup="tooltip" title="" data-placement="top" id="top" data-original-title="Project Name"><b>Project Name: </b>{{ $projectname }} <i class="icon-stack-empty   "></i> </button></h5>
						
				                @if(Session::get('roleid') == 1 )		
						<div class="header-elements">
							<div class="list-icons">
									
							
	<div class="btn-group">
			                    	<button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left dropdown-toggle" data-toggle="dropdown"><b><i class="icon-cogs"></i></b> Action</button>
			                    	<div class="dropdown-menu dropdown-menu-right">
										<a href="#" class="dropdown-item"><i class="icon-stack-check"></i> Mark Project as Completed</a>
										<a href="#" class="dropdown-item"><i class="icon-hand"></i> Mark Project as Hold</a>
										<a href="#" class="dropdown-item"><i class="icon-stack-cancel"></i> Mark Project as Canceled</a>
										<div class="dropdown-divider"></div>
										
										<a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal_create_task"><i class="icon-database-edit2 "></i> Add Task</a>
									
										<a href="{{action('ProjectController@edit',  Session::get('projectid'))}}" class="dropdown-item"><i class="icon-database-edit2 "></i> Edit Project</a>

										
									</div>

								
								</div>
				            
		                	</div>
	                	</div>
	                		@endif
					</div>

		<div class="card-body">
				<div class="row">
					<div class="col-lg-12">

					<!-- Default tabs Overview-->
					   <div class="card">
					   			<div class="card-header bg-light pb-0 pt-sm-0 header-elements-sm-inline">
								<ul class="nav nav-tabs nav-tabs-highlight card-header-tabs text-left">
										
									
										<li class="nav-item">
											<a href="#card-tab1" class="nav-link active" data-toggle="tab">
												<i class="icon-screen-full mr-2"></i>
												Overview
											</a>
										</li>

										@if(Session::get('roleid') == 1 )	
										<li class="nav-item">
											<a href="#card-tab2" class="nav-link " data-toggle="tab">
												<i class="icon-users4 mr-2"></i>
												Add Members
											</a>
										</li>
										@endif
											<li class="nav-item">
											<a href="#card-tab3" class="nav-link " data-toggle="tab">
												<i class="icon-stats-bars mr-2"></i>
												Task List
											</a>
										</li>

									
									</ul>
								<h6 class="card-title"></h6>
								<div class="header-elements">
									
									<div class="header-elements" style="margin-left:20px;">
							<div class="list-icons">
									 

		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
			                	</div>
							</div>
							
					<div class="card-body tab-content">
					  <div class="tab-pane fade  show active" id="card-tab1">
                        @include('project.overview')
					   </div>
                    {{-- End Tab Overview  --}}

					{{-- Tab Add Members --}}

				       <div class="tab-pane fade" id="card-tab2">
				       	@include('project.members')
					   </div>
					{{-- End Tab Add Members --}}

					{{-- Tab Task List --}}
						<div class="tab-pane fade" id="card-tab3">
						@include('tasks.tab')
						</div>
					{{-- End Tab Task List --}}
				     </div>
			    </div>
	      </div>
	   </div>
	 </div>

		@endsection