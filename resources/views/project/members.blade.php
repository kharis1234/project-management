	<div class="card">
					

					<div class="card-body">
								@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
								@endif

					
								@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
								@endif

						<form class="form-validate-jquery" action="{{action('ProjectController@addmembers', Session::get('projectid'))}}" method="post"> {{ csrf_field() }}
							<fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Form Field</legend>
								
								<!-- Password field -->
								
								<div class="form-group">
									<label>Users</label>
									<select multiple="multiple" id="members" name="members[]" class="form-control select" data-container-css-class="border-primary text-primary-700" data-dropdown-css-class="border-primary" data-fouc>

							@foreach( $listuser['data'] as $listusers)
										<option value="{{ $listusers['id']}}" {{ (collect(old('members'))->contains($listusers['id'])) ? 'selected':'' }} 
              				 {{ (in_array($listusers['id'],$membersexplode)) ? 'selected' : ''}} 
              				  >{{ $listusers['employee_name']}} - {{ $listusers['email']}}</option>
							@endforeach
	
									</select>
								</div>

							

							</fieldset>

							<div class="d-flex justify-content-end align-items-center">
								<button type="reset" class="btn btn-light" id="reset">Reset <i class="icon-reload-alt ml-2"></i></button>
								<button type="submit" class="btn btn-primary ml-3">Save<i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>