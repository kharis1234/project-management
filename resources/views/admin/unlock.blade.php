@extends('layouts.adminLayout.admin_design_unlock')
	
	@section('js_unlock_page')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/login.js') }}"></script>
   
    @endsection

	@section ('content')


	<div class="content d-flex justify-content-center align-items-center">

				<!-- Unlock form -->
				<form class="login-form" action="{{ url('prosesUnlock') }}" method="post">

					{{ csrf_field() }}

					@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
					

					@endif

					@if(Session::has('flash_message_error2'))
								<div class="alert alert-info alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error2') !!}</span>
							    </div>
					

					@endif

					@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
					

					@endif

					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center">
								<div class="card-img-actions d-inline-block mb-3">
									<img class="rounded-circle" src="{{ asset('global_assets/backend/images/placeholders/placeholder.jpg') }}" width="160" height="160" alt="">
									<div class="card-img-actions-overlay card-img rounded-circle">
										<a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
											<i class="icon-question7"></i>
										</a>
									</div>
								</div>
							</div>

							<div class="text-center mb-3">
								<h6 class="font-weight-semibold mb-0">{{Session::get('employeename')}}</h6>
								<span class="d-block text-muted">Unlock your account</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-right">
								<input type="password" name="password" class="form-control" placeholder="Your password">
								<div class="form-control-feedback">
									<i class="icon-user-lock text-muted"></i>
								</div>
							</div>

							<div class="form-group d-flex align-items-center">
								<div class="form-check mb-0">
									<label class="form-check-label">
										<input type="checkbox" name="remember" class="form-input-styled" data-fouc>
										Remember
									</label>
								</div>

								<a href="#" class="ml-auto">Forgot password?</a>
							</div>

							<button type="submit" class="btn btn-primary btn-block"><i class="icon-unlocked mr-2"></i> Unlock</button>
						</div>
					</div>
				</form>
				<!-- /unlock form -->

			</div>

		@endsection