	@extends ('layouts.adminLayout.admin_design')
	
	@section('js_dashboard')
	<script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/velocity/velocity.ui.min.js') }}"></script>
	<script src="{{ asset('global_assets/backend/js/demo_pages/animations_velocity_examples.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/session_timeout.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_idle_timeout.js') }}"></script>
	@endsection

	@section ('content')

	<div class="content">
<div class="row">
	            
					<div class="col-sm-6 col-xl-4">
						<a href="{{ url('project') }}" >
						<div class="card card-body bg-blue-400 has-bg-image">
							<div class="media">
								<div class="media-body">
									<h3 class="mb-0">{{ $projects['total_data'] }}</h3>
									<span class="text-uppercase font-size-xs">Open Projects</span>
								</div>

								<div class="ml-3 align-self-center">
									<i class="icon-database-menu  icon-3x opacity-75"></i>
								</div>
							</div>
						</div>
					  </a>
					</div>
				

					<div class="col-sm-6 col-xl-4">
						@if(Session::get('roleid') != 1)
						<a href="{{ url('tasks') }}" >
						@endif
						<div class="card card-body bg-danger-400 has-bg-image">
							<div class="media">
								<div class="media-body">
									<h3 class="mb-0">
									{{-- @foreach($projectstasks['data'] as $projecttask)
										@if($projecttask['task_status_id'] !=5) --}}
										{{	count($projectstasks['data'])  }}
									{{-- 	{{ count($projecttask) }} --}}
									{{-- 	@endif
									@endforeach --}}
								</h3>
									<span class="text-uppercase font-size-xs">Open Tasks</span>
								</div>

								<div class="ml-3 align-self-center">
									<i class="icon-inbox  icon-3x opacity-75"></i>
								</div>
							</div>
						</div>
						@if(Session::get('roleid') != 1)
					</a>
					@endif
					</div>

					<div class="col-sm-6 col-xl-4">
						<div class="card card-body bg-success-400 has-bg-image">
							<div class="media">
								<div class="mr-3 align-self-center">
									<i class="icon-task  icon-3x opacity-75"></i>
								</div>

								<div class="media-body text-right">
									<h3 class="mb-0">{{	count($projecttaskcompleted['data'])  }}</h3>
									<span class="text-uppercase font-size-xs">Project Completed</span>
								</div>
							</div>
						</div>
					</div>

				{{-- 	<div class="col-sm-6 col-xl-3">
						<div class="card card-body bg-indigo-400 has-bg-image">
							<div class="media">
								<div class="mr-3 align-self-center">
									<i class="icon-enter6 icon-3x opacity-75"></i>
								</div>

								<div class="media-body text-right">
									<h3 class="mb-0">245,382</h3>
									<span class="text-uppercase font-size-xs">total visits</span>
								</div>
							</div>
						</div>
					</div> --}}
				</div>
	</div>
	@endsection