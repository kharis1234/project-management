	@extends ('layouts.adminLayout.admin_design')
	@section ('content')

<div class="content">

				<!-- Form validation -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Update Password</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">
								@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
								@endif

					
								@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
								@endif

						<form class="form-validate-jquery" action="{{ url('/admin/update-pwd') }}" method="post"> {{ csrf_field() }}
							<fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Form Field</legend>
								


								<!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Current Password <span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="currentpassword" id="currentpassword" class="form-control" required placeholder="Please insert current password"><span id="pwdChk"></span>
									</div>
								</div>
								<!-- /password field -->


								<!-- Password field -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">New Password <span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="password" id="password" class="form-control" required placeholder="Please insert new password">
									</div>
								</div>
								<!-- /password field -->


								<!-- Repeat password -->
								<div class="form-group row">
									<label class="col-form-label col-lg-3">Confirm password <span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<input type="password" name="repeat_password" class="form-control" required placeholder="Please insert confirm password">
									</div>
								</div>
								<!-- /repeat password -->



							</fieldset>

							<div class="d-flex justify-content-end align-items-center">
								<button type="reset" class="btn btn-light" id="reset">Reset <i class="icon-reload-alt ml-2"></i></button>
								<button type="submit" class="btn btn-primary ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
				<!-- /form validation -->

			</div>

	@endsection