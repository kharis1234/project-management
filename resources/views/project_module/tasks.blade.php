					<div class="col-lg-12">
				<div class="card border-primary">

							<div class="card-body">
								<ul class="nav nav-pills nav-pills-bordered">
								{{-- 	<li class="nav-item"><a href="#bordered-pill1{{ $dataprojectmodul['id'] }}" class="nav-link active" data-toggle="tab">Manage Tasks</a></li> --}}
									{{-- <li class="nav-item"><a href="#bordered-pill2{{ $dataprojectmodul['id'] }}" class="nav-link" data-toggle="tab">Assign to Developers</a></li>
									<li class="nav-item"><a href="#bordered-pill3{{ $dataprojectmodul['id'] }}" class="nav-link" data-toggle="tab">Assign to QA</a></li>
									<li class="nav-item"><a href="#bordered-pill4{{ $dataprojectmodul['id'] }}" class="nav-link" data-toggle="tab">Tasks List</a></li> --}}
									
								</ul>

								<div class="tab-content">

{{-- Tab 1 --}}
									{{-- @include('tasks.taskl') --}}
			<div class="tab-pane fade show active" id="bordered-pill1{{ $dataprojectmodul['id'] }}">

				<div class="card-header bg-primary text-white header-elements-inline ">
						 <h5 class="card-title"></h5>
						
				                		
						<div class="header-elements">
							<div class="list-icons">
									 
								<div style="margin-right:10px;"><button type="button" class="btn alpha-blue text-blue-800 border-blue-600" data-toggle="modal" data-target="#modal_create_task">
					                			
				                			Add Task <i class="icon-file-plus  "></i> </button>


				                	&nbsp;&nbsp;&nbsp; |</div> 
		                		
		                		<a class="list-icons-item" data-action="reload"></a>
		                		
		                	</div>
	                	</div>
					</div>

		<table class="table  table-hover datatable-highlight">
	
						<thead>
							
							<tr>
								{{-- <th></th>  --}}
								<th>No</th>
								<th>Title</th>
							
								<th>Assign to</th>
								<th>Start Date</th>
								<th>Deadline</th>

							{{-- 	<th>target_hours_dev</th>
								<th>target_end_date_dev</th> --}}
								{{-- <th>QA by</th> --}}
							{{-- 	<th>target_hours_qa</th>
								<th>target_end_date_qa</th> --}}
									<th>Creator</th>
								<th>Status</th>

								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden"; {{	$nourut = 1 }} >


				      @foreach($data3['data'] as $tasks)	
                          @if ($tasks['project_module_id'] == $dataprojectmodul['id'])

                          		@if($tasks['is_delete'] != 1 )


							 <tr>
								<td>{{ $nourut++ }} </td>
								<td><b>{{ $tasks['title'] }} </b></br>
									<i>{{ $tasks['description'] }} </i> </td>
								{{-- <td></td> --}}
								<td> <b>Developer by: </b> 
									
								@if (! isset($tasks['developer_assigned_by'] ) )
								<font color="red"><i>	None </i></font>
								@else
									@foreach($listuser['data'] as $listuser2)

									<font color="green">{{ $tasks['developer_id'] == $listuser2['id'] ? $listuser2['employee_name'] : ''}} </font>       									@endforeach
								@endif
        
								</br></br><b>QA by: </b> 
								@if (! isset($tasks['tester_id'] ) )
								<font color="red"><i>	None </i></font>
								@else
									@foreach($listuser['data'] as $listuser2)

									<font color="green">{{ $tasks['tester_id'] == $listuser2['id'] ? $listuser2['employee_name'] : ''}} </font>       									@endforeach
								@endif
							</td>
							
							<td>{{ $tasks['kickoff_date'] }}</td>
							<td>{{ $tasks['target_end_date_dev'] }}</td>
							
						
								<td>{{ $tasks['creator'] }} </td>
									<td>{{ $tasks['status_desc'] }} </td>
							
								
								
								<td class="text-center">
							<div class="list-icons">
											<div class="dropdown">
												{{-- <a href="" class="list-icons-item" data-popup="tooltip" title="View" data-container="body">
					                			<i class="icon-search4 "></i>
				                			</a> --}}
						                		<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog6"></i></a>

												<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
													<div class="dropdown-header">Options</div>
												{{-- 	<a href="#" class="dropdown-item list-icons-item text-primary-600" ><i class="icon-pencil7"></i>Edit</a> --}}


												<button type="button" class="dropdown-item list-icons-item text-primary-600" data-toggle="modal" data-target="#modal_edit_task{{ $tasks['id']}}">
					                			
				                			<i class="icon-pencil7"></i>Edit </button>
													
													{{-- <button class="dropdown-item list-icons-item text-danger-600" {{-- data-catid={{$cat->id}} --}} {{-- data-toggle="modal" data-target="#delete{{ $tasks['id'] }}"><i class="icon-bin"></i>Delete</button> --}} 




													<a href="{{ action('TaskController@destroy', $tasks['id'])}}" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Delete</a>
													
												</div>
					                		</div>
					                	</div>
								</td>

   
							   </tr>
 												@endif
											@endif

											
											
									@endforeach
									</tbody>
					</table>

			</div>
{{-- End Tab 1 --}}

{{-- Tab 2 --}}

									<div class="tab-pane fade" id="bordered-pill2{{ $dataprojectmodul['id'] }}">
								
									<table class="table  datatable-basic table-striped">
	
						<thead>
							
							<tr>
								{{-- <th></th> --}}
								<th>No</th>
								<th>Title</th>
								<th>Description</th>
								<th>QA by</th>
								<th>Target Hours/Real</th>
								<th>End Date/Real</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							
					@foreach($data3['data'] as $tasks)
			     	    @if ($tasks['project_module_id'] == $dataprojectmodul['id'])

										
													<tr>
													{{-- 	<td></td> --}}
									<td>{{ $nourut++ }} </td>
									<td>{{ $tasks['title'] }}</td>
									<td>{{ $tasks['description'] }} </td>
							
								<td>
									
								@if (! isset($tasks['developer_id'] ) )
									<button type="button" class="btn btn-outline-primary rounded-round" data-toggle="modal" data-target="#modal_theme_primary">Assign To<i class="icon-user  ml-1"></i></button>
								@else
									@foreach($listuser['data'] as $listuser2)

									{{ $tasks['developer_id'] == $listuser2['id'] ? $listuser2['employee_name'] : ''}}
									
									@endforeach

									<br/><a href="#" class="dropdown-item list-icons-item text-primary-600"><i class="icon-pencil7"></i>Edit</a>
									<a href="#" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Remove</a>

								@endif


						


							</td>

								<td>{{ $tasks['target_hours_qa'] }}/{{ $tasks['target_hours_qa_real'] }} </td>
								<td>{{ $tasks['target_end_date_qa'] }}/{{ $tasks['target_end_date_qa_real'] }} </td>
					
								<td class="text-center">
						<button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" ><b><i class="icon-arrow-right5  "></i></b> Send</button>
								</td>

   
							   </tr>

										

											
											@endif
									@endforeach
									</tbody>
					</table>
								
							
									</div>
{{-- End Tab 2 --}}

{{-- Tab 3 --}}



									<div class="tab-pane fade" id="bordered-pill3{{ $dataprojectmodul['id'] }}">
								
									<table class="table  datatable-basic table-striped">
	
						<thead>
							
							<tr>
								{{-- <th></th> --}}
								<th>No</th>
								<th>Title</th>
								<th>Description</th>
								<th>QA by</th>
								<th>Target Hours/Real</th>
								<th>End Date/Real</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden"; {{	$nourut = 1 }} >
									@foreach($data3['data'] as $tasks)
			     	    @if ($tasks['project_module_id'] == $dataprojectmodul['id'])

										
													<tr>
													{{-- 	<td></td> --}}
									<td>{{ $nourut++ }} </td>
									<td>{{ $tasks['title'] }}</td>
									<td>{{ $tasks['description'] }} </td>
							
								<td>
									
								@if (! isset($tasks['developer_id'] ) )
									<button type="button" class="btn btn-outline-primary rounded-round" data-toggle="modal" data-target="#modal_theme_primary2">Assign To<i class="icon-user  ml-1"></i></button>
								@else
									@foreach($listuser['data'] as $listuser2)

									{{ $tasks['developer_id'] == $listuser2['id'] ? $listuser2['employee_name'] : ''}}
									
									@endforeach

									<br/><a href="#" class="dropdown-item list-icons-item text-primary-600"><i class="icon-pencil7"></i>Edit</a>
									<a href="#" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Remove</a>

								@endif


						


							</td>

								<td>{{ $tasks['target_hours_qa'] }}/{{ $tasks['target_hours_qa_real'] }} </td>
								<td>{{ $tasks['target_end_date_qa'] }}/{{ $tasks['target_end_date_qa_real'] }} </td>
					
								<td class="text-center">
						<button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" ><b><i class="icon-arrow-right5  "></i></b> Send</button>
								</td>

   
							   </tr>

										

											
											@endif
									@endforeach
									</tbody>
					</table>
						
									</div>
	{{-- End Tab 3 --}}
	{{-- Tab 4 --}}

									<div class="tab-pane fade" id="bordered-pill4{{ $dataprojectmodul['id'] }}">
											<table class="table  table-hover datatable-highlight">
						<thead>
							<tr>
								{{-- <th></th> --}}
								<th>No</th>
								<th>Title</th>
								<th>Description</th>
								<th>Target Hours</th>
								<th>Target Hours Real</th>
								<th>End Date</th>
								<th>End Date Real</th>
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<input type="hidden"; {{	$nourut = 1 }} >
					@foreach($data3['data'] as $tasks)
			     	    @if ($tasks['project_module_id'] == $dataprojectmodul['id'])
							{{-- @if($dataprojectmodule['is_delete'] == 0  ) --}}
									<tr>
													{{-- 	<td></td> --}}
									<td>{{ $nourut++ }} </td>
								<td>{{ $tasks['title'] }} </td>
									<td>{{ $tasks['description'] }} </td>
									<td>{{ $tasks['target_hours_dev'] }} </td>
									<td>{{ $tasks['target_hours_dev_real'] }} </td>
									<td>{{ $tasks['target_end_date_dev'] }} </td>
									<td>{{ $tasks['target_end_date_dev_real'] }} </td>
									<td>{{ $tasks['status_desc'] }} </td>
							{{-- 
								<td>
							<span class="badge badge-danger">{{ isset($tasks['developer_assigned_by']) ? $tasks['developer_assigned_by'] : 'Assign to' }}</span>
								


							</td> --}}
					
								<td class="text-center">
						<button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" ><b><i class="icon-arrow-right5  "></i></b> Submit</button>
								</td>

   
							   </tr>


							   @endif
							@endforeach
						
							
						</tbody>
					</table>

									</div>
		{{-- End Tab 4 --}}

							


								</div>
							</div>
						</div>


					

					</div>
				</div>
				
				

					
				
				<!-- /Data tables Responsive -->
