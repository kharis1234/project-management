	@extends ('layouts.adminLayout.admin_design')
	@section('js_datatables')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/contextmenu.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/table_elements.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/trees/fancytree_childcounter.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_trees.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_basic.js') }}"></script>
   
     <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/form_inputs.js') }}"></script>
        <script src="{{ asset('global_assets/backend/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/select2.min.js') }}"></script>
   <script src="{{ asset('global_assets/backend/js/demo_pages/components_modals.js') }}"></script>  

   	   <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/anytime.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.date.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/pickers/pickadate/legacy.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/plugins/notifications/jgrowl.min.js') }}"></script>    
    <script src="{{ asset('global_assets/backend/js/demo_pages/picker_date.js') }}"></script>
     <script src="{{ asset('global_assets/backend/js/demo_pages/form_checkboxes_radios.js') }}"></script>  
     <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/select2.min.js') }}"></script>
      <script src="{{ asset('global_assets/backend/js/demo_pages/form_select2.js') }}"></script> 

  

    @endsection

	@section ('content')

<div class="content">

				{{-- Section 1 --}}
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title"><i class="icon-grid6 "></i> {{$projectmodulename }}</h5>

					
						
						

					</div>

					<div class="card-body">
								@if(Session::has('flash_message_error'))
								<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_error') !!}</span>
								</div>
								@endif

					
								@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">{!! session('flash_message_success') !!}</span>
							    </div>
								@endif

					
@foreach($data['data'] as $dataprojectmodul)
				

							@endforeach
					</div>
				</div>
{{-- End Section 1 --}}
				<!-- /form validation -->
{{-- Section 2 --}}
			<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Tasks</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
			<div class="card-body">
				@include('project_module.tasks')
   
			</div>	
{{-- End Section 2 --}}

			</div>



			   <!-- Assign to Developer -->
				<div id="modal_theme_primary" class="modal fade" tabindex="-1" style="z-index:1051;">
						<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-user  mr-2"></i> &nbsp;List User</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body">
						<table class="table datatable-basic table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Gitlab User</th>
								{{-- <th>Status</th> --}}
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
									<input type="hidden"; {{	$nourut = 1 }} >
							@foreach($listuser['data'] as $user)
									{{-- @foreach($listrole['data'] as $role) --}}

											{{-- @if($role['id'] == 4) --}}

												<tr>
								<td>{{ $nourut++ }}</td>
								<td>{{ $user['nip']}}</td>
								<td>{{ $user['employee_name']}}</td>
								{{--  <td>{{ $user['role_name']}}</td> --}}
								<td>{{ $user['email']}}</td>
								<td>{{ $user['gitlab_username']}}</td> 
								{{-- <td>{{ $user['user_is_active']}}</td>  --}}
								<td class="text-center">
										<button data-dismiss="modal" type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" ><b><i class="icon-reply " ></i></b> Select</button>
								</td>
							</tr>

											{{-- @endif --}}


						
							{{--  @endforeach --}}
							@endforeach
						</tbody>
					</table>
							</div>

							
						</div>
					</div>
				</div>
				<!-- /primary modal -->

				  <!-- Assign to QA -->
					<div id="modal_theme_primary2" class="modal fade" tabindex="-1" style="z-index:1051;">
						<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-user  mr-2"></i> &nbsp;List User</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<div class="modal-body">
						<table class="table datatable-basic table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Gitlab User</th>
								{{-- <th>Status</th> --}}
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
									<input type="hidden"; {{	$nourut = 1 }} >
							@foreach($listuser['data'] as $user)
									{{-- @foreach($listrole['data'] as $role) --}}

											{{-- @if($role['id'] == 4) --}}

												<tr>
								<td>{{ $nourut++ }}</td>
								<td>{{ $user['nip']}}</td>
								<td>{{ $user['employee_name']}}</td>
								{{--  <td>{{ $user['role_name']}}</td> --}}
								<td>{{ $user['email']}}</td>
								<td>{{ $user['gitlab_username']}}</td> 
								{{-- <td>{{ $user['user_is_active']}}</td>  --}}
								<td class="text-center">
										<button data-dismiss="modal" type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round" ><b><i class="icon-reply " ></i></b> Select</button>
								</td>
							</tr>

											{{-- @endif --}}


						
							{{--  @endforeach --}}
							@endforeach
						</tbody>
					</table>
							</div>

							
						</div>
					</div>
				</div>
				<!-- /primary modal -->

				   <!-- Create a new Tasks -->


				<div id="modal_create_task" class="modal fade" tabindex="-1" style="z-index:1051;">
						<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-task  mr-2"></i> &nbsp;Add Task</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<form class="form-validate-jquery form-horizontal" action="{{ url('/tasks/store') }}" method="post"> {{ csrf_field() }}
			
								<div class="modal-body">
									<input type="hidden" name="projectmoduleid" id="projectmoduleid" placeholder="title" class="form-control" value="{{ $projectmoduleid }}" >
									
									  <div class="form-group row">
		                        	<label class="col-form-label col-sm-3">Parent Tasks</label>
		                        	<div class="col-sm-9">
			                            <select class="custom-select" id="parenttasksid" name="parenttasksid">
									<option value="null">- Select Parent Task -</option>
			                            	@foreach($data3['data'] as $parenttasks )
			                            		@if( $parenttasks['project_module_id'] == $projectmoduleid && $parenttasks['is_delete'] == 0)
												<option value={{ $parenttasks['id'] }}>{{ $parenttasks['title'] }} - {{ $parenttasks['description'] }}
												</option>
												@endif
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>
	

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Insert a Title</label>
										<div class="col-sm-9">
											<input type="text" name="title" id="title" placeholder="title" class="form-control">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Description</label>
										<div class="col-sm-9">
											<input type="text" name="desc" id="desc" placeholder="Description" class="form-control">
										</div>
									</div>

									 <div class="form-group row">
		                        	<label class="col-form-label col-sm-3">Assign to Developer</label>
		                        	<div class="col-sm-9">
			                            <select class="custom-select" id="developerassignedby" name="developerassignedby">
									<option value="null">- Select Developer -</option>
			                            	@foreach($listuser['data'] as $listusers )
			                            	
												<option value={{ $listusers['id'] }}>{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
												
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>
		                      <div class="form-group row">
									<label class="col-form-label col-sm-3">Start Date  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single"  placeholder="Please insert kickoff date">
									</div>
								</div>
									<div class="form-group row">
									<label class="col-form-label col-sm-3">Deadline  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date_dev" id="target_end_date_dev" class="form-control daterange-single"  placeholder="Please insert Deadline">
									</div>
								</div>

							
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="submit" class="btn bg-primary">Save</button>
								</div>
							</form>

							
						</div>
					</div>
				</div>
			   <!-- Create end a new Tasks -->


			      <!-- Edit a task -->
			      @foreach ($data3['data'] as $tasks)
				<div id="modal_edit_task{{ $tasks['id']}}" class="modal fade" tabindex="-1" style="z-index:1051;">
						<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-task  mr-2"></i> &nbsp;Edit a task - </h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<form class="form-validate-jquery form-horizontal" action="{{ action('TaskController@update', $tasks['id'])}}" method="post"> {{ csrf_field() }}
			
								<div class="modal-body">
									<input type="hidden" name="projectmoduleid" id="projectmoduleid" placeholder="title" class="form-control" value="{{ $projectmoduleid }}" >
									 <div class="form-group row">
		                        	<label class="col-form-label col-lg-3">Parent Task</label>
		                        	<div class="col-lg-9">
			                            <select class="custom-select" id="parenttasksid" name="parenttasksid">
									<option value="null">- Select Parent Task -</option>
			                            	@foreach($data3['data'] as $parenttasks )
			                            	@if( $parenttasks['project_module_id'] == $projectmoduleid && $parenttasks['is_delete'] == 0)
			                            	<option value="{{ $parenttasks['parent_id'] }}"  
			                            	{{ $parenttasks['parent_id'] != null ? 'selected':'' }}>{{ $parenttasks['title'] }} - {{ $parenttasks['description'] }}
												</option>
												@endif
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Insert a Title</label>
										<div class="col-sm-9">
											<input type="text" name="title" id="title" placeholder="title" class="form-control" value="{{ $tasks['title'] }}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Description</label>
										<div class="col-sm-9">
											<input type="text" name="desc" id="desc" placeholder="Description" class="form-control" value="{{ $tasks['description'] }}">
										</div>
									</div>

								@if($tasks['task_status_id'] == 5)

								  <div class="form-group row">
		                        	<label class="col-form-label col-sm-3">Assign to QA </label>
		                        	<div class="col-sm-9">

		                        		 <select class="custom-select" id="testerid" name="testerid">
									<option value="null">- Select QA -</option>
			                            	@foreach($listuser['data'] as $listusers )
			                            	<option value="{{ $listusers['id'] }}"  
			                            	{{ $listusers['id'] == $tasks['tester_id'] ? 'selected':'' }}>
			                            	{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
			                            	@endforeach
			                            </select>

			                           
		                            </div>
		                        </div>

		                       <input type="hidden" name="developerid" id="developerid" placeholder="title" class="form-control" value="{{ $tasks['developer_id'] }}">


								@else

								  <div class="form-group row">
		                        	<label class="col-form-label col-sm-3">Assign to Developer</label>
		                        	<div class="col-sm-9">

		                        		 <select class="custom-select" id="developerid" name="developerid">
									<option value="null">- Select Developer -</option>
			                            	@foreach($listuser['data'] as $listusers )
			                            	<option value="{{ $listusers['id'] }}"  
			                            	{{ $listusers['id'] == $tasks['developer_id'] ? 'selected':'' }}>
			                            	{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
			                            	@endforeach
			                            </select>

			                           
		                            </div>
		                        </div>


								@endif

						

		                        
		                             <div class="form-group row">
									<label class="col-form-label col-sm-3">Kick Off Date  <span class="text-danger">*</span></label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single" required placeholder="Please insert project" value="{{ \Carbon\Carbon::parse($tasks['kickoff_date'])->format('m/d/Y') }}">

									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-sm-3">Target End Date  <span class="text-danger">*</span></label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single" required placeholder="Please insert project" value="{{ \Carbon\Carbon::parse($tasks['target_end_date_dev'])->format('m/d/Y') }}">
									</div>
								</div>

							
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="submit" class="btn bg-primary">Save</button>
								</div>
							</form>

							
						</div>
					</div>
				</div>
				@endforeach
			   <!-- Create end a new Tasks -->


@foreach ($data3['data'] as $tasks)
				 <div class="modal modal-danger fade" id="delete{{ $tasks['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Delete Confirmation</h4>
      </div>
      <form action="{{ action('TaskController@destroy', $tasks['id'])}}" method="post">
      	{{-- 	{{method_field('delete')}} --}}
      		{{-- {{csrf_field()}} --}}
	      <div class="modal-body">

	{{--       	<input type="hidden" name="id" id="projectmoduleid" placeholder="title" class="form-control" value="{{ $projectmoduleid }}" > --}}


				<p class="text-center">
					Are you sure you want to delete this?
				</p>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-success" data-dismiss="modal">No, Cancel</button>
	        <button type="submit" class="btn btn-warning">Yes, Delete</button>
	      </div>
      </form>
    </div>
  </div>
</div>

@endforeach


	@endsection