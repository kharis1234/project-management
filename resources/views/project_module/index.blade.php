@extends ('layouts.adminLayout.admin_design')
	
	@section('js_datatables')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/contextmenu.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/table_elements.js') }}"></script>
   

    @endsection

    @section('js_dashboard')
	
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/session_timeout.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_idle_timeout.js') }}"></script>
	@endsection

	@section ('content')


	<div class="content">
		<!-- Data tables Responsive -->
				<div class="card">
					<div class="card-header bg-info text-white header-elements-inline">
						 <h5 class="card-title"><button type="button" class="btn badge-striped" data-popup="tooltip" title="" data-placement="top" id="top" data-original-title="Project Name"><b>Project Name: </b>{{ $projectname }} <i class="icon-stack-empty   "></i> </button></h5>
						
				                		
						<div class="header-elements">
							<div class="list-icons">
									<div style="margin-right:10px;"><a href="{{action('ModuleController@create', Session::get('projectid') )}}"><button type="button" class="btn alpha-blue text-blue-800 border-blue-600">
					                			
				                			Add a module <i class="icon-file-plus  "></i> </button></a>&nbsp;&nbsp;&nbsp; |</div> 

		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

				
					<ul style="margin-left: -40px;">

					<table class="table datatable-responsive-row-control table-hover datatable-highlight">
						<thead>
							<tr>
								<th></th>
								<th>Module Name</th>
								<th>Description</th>
								{{-- <th>Project ID</th> --}}
								<th>Module Status ID</th>
								<th>Target End Date</th>
								<th>Target End Date Actually</th>
								<th>Creator</th>
								<th>Status Module</th>
								<th>Active/Inactive</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							
							@foreach($data['data'] as $dataprojectmodule)
							@if($dataprojectmodule['is_delete'] == 0  )
							<tr>
									<td></td>
								<td>{{ $dataprojectmodule['module_name'] }} </td>
								<td>{{ $dataprojectmodule['description'] }} </td>
								{{-- <td>{{ $projectname }} </td> --}}
								<td> {{ $dataprojectmodule['module_status_id'] }} </td>

								<td>{{ \Carbon\Carbon::parse($dataprojectmodule['target_end_date'])->format('d/m/Y')}}</td>
								<td>{{ \Carbon\Carbon::parse($dataprojectmodule['target_end_date_real'])->format('d/m/Y')}}</td>
								<td>{{ $dataprojectmodule['creator'] }} </td>
								<td>{{ $dataprojectmodule['status_desc'] }} </td>
								<td>
								@if( $dataprojectmodule['is_active'] == 0)
									<span class="badge badge-secondary">Inctive</span>
								@elseif($dataprojectmodule['is_active'] == 1)
									<span class="badge badge-success">Active</span>
								@else
									<span class="badge badge-info">Pending</span>
								@endif
								</td>
								<td class="text-center">
								<div class="list-icons">
					                		<a href="{{action('ModuleController@edit', $dataprojectmodule['id'])}}" class="list-icons-item" data-popup="tooltip" title="Edit" data-container="body">
					                			<i class="icon-pencil7"></i>
				                			</a>
					                		
					                		<a href="" class="list-icons-item" data-popup="tooltip" title="View" data-container="body">
					                			<i class="icon-search4 "></i>
				                			</a>
				                			<a href="{{action('ModuleController@destroy', $dataprojectmodule['id'])}}" class="list-icons-item" data-popup="tooltip" title="Remove" data-container="body">
					                			<i class="icon-trash"></i>
				                			</a>
					                	</div>
								</td>

   
							   </tr>

							   @endif
							@endforeach
						
							
						</tbody>
					</table>
				</div>
				<!-- /Data tables Responsive -->

			</div>

		@endsection