<table class="table  table-hover datatable-highlight">
						<thead>
							<tr>
								<th>No</th>
								<th>Task Name</th>
								<th>Start Date</th>
								<th>Deadline</th>
								<th>Assigned to</th>
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						<input type='hidden' value='{{  $no=1 }}'>	</input>
							@foreach($tasks['data'] as $task)
							@if(Session::get('roleid') == 1)

							@if($task['is_delete'] == 0  && $task['project_module_id'] == $projectid)
							<tr>
									<td>{{ $no++ }}</td>
						
								<td>
									<b>{{ $task['title'] }}</b> </br> {{ $task['description'] }}   
							</td>
							<td>
									{{ \Carbon\Carbon::parse($task['kickoff_date'])->format('d/m/Y')}}
							</td>

								<td>
									{{ \Carbon\Carbon::parse($task['target_end_date'])->format('d/m/Y')}}
							</td>
							<td width="250">
								 <b>Dev by: </b> 
									
								@if (! isset($task['developer_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['developer_name']}} <i class="icon-user  mr-2"></i> </font></br>

									@foreach($listuser['data'] as $listuser2)

									<font style="font-size:11px; font-style: italic; ">{{ $task['developer_assigned_by'] == $listuser2['id'] ? '( Assigned by '. $listuser2['employee_name'] .' )' : ''}}  </font>       									@endforeach
       									
								@endif
        
								</br></br><b>QA by: </b> 
								@if (! isset($task['tester_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['tester_name']}} <i class="icon-user  mr-2"></i></font></br>

									@foreach($listuser['data'] as $listuser2)

									<font style="font-size:11px; font-style: italic; ">{{ $task['tester_assigned_by'] == $listuser2['id'] ? '( Assigned by '. $listuser2['employee_name'] .' )' : ''}}  </font>       									@endforeach 									
								@endif</td>
								<td> 
									@switch( $task['task_status_id'])
										@case(1)
										<span class="badge badge-light badge-striped badge-striped-left border-left-success">{{ $task['status_desc'] }}</span>

										@break

										@case(5)
										<span class="badge badge-light badge-striped badge-striped-left border-left-warning">{{ $task['status_desc'] }}</span>
										@break

										@default
										<span class="badge badge-light badge-striped badge-striped-left border-left-primary">{{ $task['status_desc'] }}</span>
										
									@endswitch
								</td>
								<td class="text-center">
							<div class="list-icons">
											<div class="dropdown">
												
						                		<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog6"></i></a>

												<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
											
												<button type="button" class="dropdown-item list-icons-item text-primary-600" data-toggle="modal" data-target="#modal_edit_task{{ $task['id']}}">
					                			
				                			<i class="icon-pencil7"></i>Edit </button>

				                			<button type="button" class="dropdown-item list-icons-item " data-toggle="modal" data-target="#modal_view_task{{ $task['id']}}">
					                			
				                			<i class="icon-zoomin3"></i>View </button>
											
													<a href="{{ action('TaskController@destroy', $task['id'])}}" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Delete</a>
													
												</div>
					                		</div>
					                	</div>
								</td>
						
							   </tr>

							   @endif

							@else 

							@if($task['is_delete'] == 0  && $task['tester_id'] == Session::get('userid') || $task['developer_id'] == Session::get('userid')  && $task['project_module_id'] == $projectid)
							<tr>
									<td>{{ $no++ }}</td>
						
								<td>
									<b>{{ $task['title'] }}</b> </br> {{ $task['description'] }}   
							</td>
							<td>
									{{ \Carbon\Carbon::parse($task['kickoff_date'])->format('d/m/Y')}}
							</td>

								<td>
									{{ \Carbon\Carbon::parse($task['target_end_date'])->format('d/m/Y')}}
							</td>
							<td width="250">
								 <b>Dev by: </b> 
									
								@if (! isset($task['developer_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['developer_name']}} <i class="icon-user  mr-2"></i> </font></br>

									@foreach($listuser['data'] as $listuser2)

									<font style="font-size:11px; font-style: italic; ">{{ $task['developer_assigned_by'] == $listuser2['id'] ? '( Assigned by '. $listuser2['employee_name'] .' )' : ''}}  </font>       									@endforeach
       									
								@endif
        
								</br></br><b>QA by: </b> 
								@if (! isset($task['tester_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['tester_name']}} <i class="icon-user  mr-2"></i></font></br>

									@foreach($listuser['data'] as $listuser2)

									<font style="font-size:11px; font-style: italic; ">{{ $task['tester_assigned_by'] == $listuser2['id'] ? '( Assigned by '. $listuser2['employee_name'] .' )' : ''}}  </font>       									@endforeach 									
								@endif</td>
								<td> 
									@switch( $task['task_status_id'])
										@case(1)
										<span class="badge badge-light badge-striped badge-striped-left border-left-success">{{ $task['status_desc'] }}</span>

										@break

										@case(5)
										<span class="badge badge-light badge-striped badge-striped-left border-left-warning">{{ $task['status_desc'] }}</span>
										@break

										@default
										<span class="badge badge-light badge-striped badge-striped-left border-left-primary">{{ $task['status_desc'] }}</span>
										
									@endswitch
								</td>
								<td class="text-center">
							<div class="list-icons">
								{{-- @if( $task['task_status_id'] == ) --}}
											<div class="dropdown">
												
						                		<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog6"></i></a>

												<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
											
												<button type="button" class="dropdown-item list-icons-item text-primary-600" data-toggle="modal" data-target="#modal_edit_task{{ $task['id']}}">
					                			
				                			<i class="icon-pencil7"></i>Edit </button>
				                			@if(Session::get('roleid') == 1 )
				                			<button type="button" class="dropdown-item list-icons-item " data-toggle="modal" data-target="#modal_view_task{{ $task['id']}}">
					                			
				                			<i class="icon-zoomin3"></i>View </button>
											
													<a href="{{ action('TaskController@destroy', $task['id'])}}" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Delete</a>
											@else
											@endif
													
												</div>
					                		</div>
					                {{-- @else
					                @endif --}}
					                	</div>
								</td>
						
							

   
							   </tr>

							   @endif


							@endif

							
							@endforeach
						
							
						</tbody>
					</table>