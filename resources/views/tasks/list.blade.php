@extends ('layouts.adminLayout.admin_design')
	
	@section('js_datatables')

   {{--  <script src="{{ asset('global_assets/backend/js/plugins/ui/ripple.min.js') }}"></script>  --}}

    <script src="{{ asset('global_assets/backend/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/uniform.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/contextmenu.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>  
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_responsive.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/datatables_advanced.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/table_elements.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/components_popups.js') }}"></script>
   
    @endsection

    @section('js_dashboard')
	
    <script src="{{ asset('global_assets/backend/js/plugins/extensions/session_timeout.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/extra_idle_timeout.js') }}"></script>
	@endsection

	@section ('content')


	<div class="content">
		<!-- Data tables Responsive -->
				<div class="card">
			
					<ul style="margin-left: -40px;">

					<table class="table datatable-responsive-row-control table-hover datatable-highlight">
						<thead>
							<tr>
								<th>No</th>
								<th>Task Name</th>
								<th>Project Name</th>
								<th>Start Date</th>
								<th>Deadline</th>
								<th>Assigned to</th>
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
							<tbody>
						<input type='hidden' value='{{  $no=1 }}'>	</input>
							@foreach($tasks['data'] as $task)
							@if($task['tester_id'] == Session::get('userid')|| $task['developer_id'] == Session::get('userid') && $task['is_delete'] == 0)

							<tr>
									<td>{{ $no++ }}</td>
						
								<td>
									<b>{{ $task['title'] }}</b> </br> {{ $task['description'] }}   
							</td>
							<td><a href="{{action('ProjectdetailController@index', $task['project_module_id'] )}}" class="list-icons-item" style="font-color:blue; font-weight: 600;" data-popup="tooltip" title="Module" data-container="body">{{ $task['project_name'] }}</a>


							</td>
							<td>
									{{ \Carbon\Carbon::parse($task['kickoff_date'])->format('d/m/Y')}}
							</td>

								<td>
									{{ \Carbon\Carbon::parse($task['target_end_date'])->format('d/m/Y')}}
							</td>
							<td width="250">
								 <b>Dev by: </b> 
									
								@if (! isset($task['developer_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['developer_name']}} <i class="icon-user  mr-2"></i> </font></br>

									@foreach($listusers['data'] as $listuser)

									<font style="font-size:11px; font-style: italic; ">{{ $task['developer_assigned_by'] == $listuser['id'] ? '( Assigned by '. $listuser['employee_name'] .' )' : ''}}  </font>       									@endforeach
       									
								@endif
        
								</br></br><b>QA by: </b> 
								@if (! isset($task['tester_id'] ) )
								<font color="red"><i>None</i></font>
								@else
									

									<font color="green">{{ $task['tester_name']}} <i class="icon-user  mr-2"></i></font></br>

									@foreach($listusers['data'] as $listuser)

									<font style="font-size:11px; font-style: italic; ">{{ $task['tester_assigned_by'] == $listuser['id'] ? '( Assigned by '. $listuser['employee_name'] .' )' : ''}}  </font>       									@endforeach 									
								@endif</td>
								<td> 
									@switch( $task['task_status_id'])
										@case(1)
										<span class="badge badge-light badge-striped badge-striped-left border-left-success">{{ $task['status_desc'] }}</span>

										@break

										@case(2)
										<span class="badge badge-light badge-striped badge-striped-left border-left-warning">{{ $task['status_desc'] }}</span>
										@break

										@case(3)
										<span class="badge badge-light badge-striped badge-striped-left border-left-violet">{{ $task['status_desc'] }}</span>
										@break

										@case(4)
										<span class="badge badge-light badge-striped badge-striped-left border-left-grey">{{ $task['status_desc'] }}</span>
										@break

											@case(5)
										<span class="badge badge-light badge-striped badge-striped-left border-left-info">{{ $task['status_desc'] }}</span>
										@break

										@default
										<span class="badge badge-light badge-striped badge-striped-left border-left-primary">{{ $task['status_desc'] }}</span>
										
									@endswitch
								</td>
								<td class="text-center">
							<div class="list-icons">
									@if(Session::get('roleid') == 3 || Session::get('roleid') == 5)
											<div class="dropdown">
												
						                		<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog6"></i></a>

												<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
											
												<button type="button" class="dropdown-item list-icons-item text-primary-600" data-toggle="modal" data-target="#modal_edit_task{{ $task['id']}}">
					                			
				                			<i class="icon-pencil7"></i>Edit </button>
										@if(Session::get('roleid') == 1)
				                			
				                			<button type="button" class="dropdown-item list-icons-item " data-toggle="modal" data-target="#modal_view_task{{ $task['id']}}">
					                			
				                			<i class="icon-zoomin3"></i>View </button>
											
													<a href="{{ action('TaskController@destroy', $task['id'])}}" class="dropdown-item list-icons-item text-danger-600"><i class="icon-bin"></i>Delete</a>
										  @endif
											
													
												</div>
					                		</div>
					                @endif
					                	</div>
								</td>
						
							   </tr>



							@endif

							
							@endforeach
						
							
						</tbody>
					</table>
				</div>
				<!-- /Data tables Responsive -->

			</div>

			@include('tasks.edit')

		@endsection