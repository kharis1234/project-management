
			     <div id="modal_create_task" class="modal fade" tabindex="-1" style="z-index:1051;">
						<div class="modal-dialog modal-full">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-task  mr-2"></i> &nbsp;Add Task</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<form class="form-validate-jquery form-horizontal" action="{{ url('/tasks/store') }}" method="post"> {{ csrf_field() }}
			
								<div class="modal-body">
									<input type="hidden" name="projectmoduleid" id="projectmoduleid" placeholder="title" class="form-control" value="{{ $projectmoduleid }}" >
									
									  <div class="form-group row">
		                        	<label class="col-form-label col-sm-2">Parent Tasks</label>
		                        	<div class="col-sm-10">
			                            <select class="custom-select" id="parenttasksid" name="parenttasksid">
									<option value="null">- Select Parent Task -</option>
			                            	@foreach($tasks['data'] as $parenttasks )
			                            		@if( $parenttasks['project_module_id'] == $projectmoduleid && $parenttasks['is_delete'] == 0)
												<option value={{ $parenttasks['id'] }}>{{ $parenttasks['title'] }} - {{ $parenttasks['description'] }}
												</option>
												@endif
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>
	

									<div class="form-group row">
										<label class="col-form-label col-sm-2">Insert a Title</label>
										<div class="col-sm-10">
											<input type="text" name="title" id="title" placeholder="title" class="form-control">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-sm-2">Description</label>
										<div class="col-sm-10">
											<input type="text" name="desc" id="desc" placeholder="Description" class="form-control">
										</div>
									</div>

									 <div class="form-group row">
		                        	<label class="col-form-label col-sm-2">Assign to Developer</label>
		                        	<div class="col-sm-10">
			                            <select class="custom-select" id="developerassignedby" name="developerassignedby">
									<option value="null">- Select Developer -</option>
			                            	@foreach($userroles['data'] as $userrole )
			                            	  @foreach($listuser['data'] as $listusers )
			                            		@if(in_array($userrole['user_id'],$membersexplode) && $listusers['id'] == $userrole['user_id'] && $userrole['role_id'] == 3   )
												<option value={{ $userrole['user_id'] }}>{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
												@endif
											 @endforeach
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>
		                      <div class="form-group row">
									<label class="col-form-label col-sm-2">Start Date  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single"  placeholder="Please insert kickoff date">
									</div>

									<label class="col-form-label col-sm-2">Target End Date Dev</label>
									<div class="col-sm-5 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single"  placeholder="Please insert kickoff date">
										<input type="text" name="target_hours_dev" id="target_hours_dev" class="form-control"  placeholder="Target Hours Dev">
									</div>
								</div>
									<div class="form-group row">
									<label class="col-form-label col-sm-2">Deadline  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single"  placeholder="Please insert Deadline">
									</div>

									<label class="col-form-label col-sm-2">Target End Date QA  </label>
									<div class="col-sm-5 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date_qa" id="target_end_date_qa" class="form-control daterange-single"  placeholder="Please insert kickoff date"> 

										<input type="text" name="target_hours_qa" id="target_hours_qa" class="form-control"  placeholder="Target Hours QA">
									</div>
								</div>

								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="submit" class="btn bg-primary">Save</button>
								</div>
							</form>

							
						</div>
					</div>
				</div>
			   