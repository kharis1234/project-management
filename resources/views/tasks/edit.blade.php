 @foreach ($tasks['data'] as $task)
				<div id="modal_edit_task{{ $task['id']}}" class="modal fade" tabindex="-1" style="z-index:1051;">

						<div class="modal-dialog {{ Session::get('roleid') == 1 ? 'modal-full':'modal-xs' }}">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><i class="icon-task  mr-2"></i> &nbsp;Task Action - </h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
						@switch(Session::get('roleid'))
   						  @case(3)
   						    <form class="form-validate-jquery form-horizontal" action="{{ action($task['task_status_id'] == 1 ? 'TaskController@taketask': 'TaskController@taskdone', $task['id'])}}" method="post">
					      @break

					      @case(5)
					       <form class="form-validate-jquery form-horizontal" action="{{ action($task['task_status_id'] == 3 ? 'TaskController@testingtask': 'TaskController@testingdone', $task['id'])}}" method="post">
					      @break

					      @default
					      	<form class="form-validate-jquery form-horizontal" action="{{ action('TaskController@update', $task['id'])}}" method="post"> 
						@endswitch

						{{ csrf_field() }}
			
								<div class="modal-body">
									@if(Session::get('roleid') == 1 )

									<input type="hidden" name="projectmoduleid" id="projectmoduleid" placeholder="title" class="form-control" value="{{ $projectmoduleid }}" >
									 <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Parent Task</label>
		                        	<div class="col-lg-10">

		                        		

			                            <select class="custom-select" id="parenttasksid" name="parenttasksid">
									<option value="null">- Select Parent Task -</option>
			                            	@foreach($tasks['data'] as $parenttasks )
			                            	@if( $parenttasks['project_module_id'] == $projectmoduleid && $parenttasks['is_delete'] == 0)
			                            	<option value="{{ $parenttasks['parent_id'] }}"  
			                            	{{ $parenttasks['parent_id'] != null ? 'selected':'' }}>{{ $parenttasks['title'] }} - {{ $parenttasks['description'] }}
												</option>
												@endif
			                            	@endforeach
			                            </select>
		                            </div>
		                        </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-2">Insert a Title</label>
										<div class="col-sm-10">
											<input type="text" name="title" id="title" placeholder="title" class="form-control" value="{{ $task['title'] }}">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-sm-2">Description</label>
										<div class="col-sm-10">
											<input type="text" name="desc" id="desc" placeholder="Description" class="form-control" value="{{ $task['description'] }}">
										</div>
									</div>

								@if($task['task_status_id'] == 5)


								  <div class="form-group row">
		                        	<label class="col-form-label col-sm-2">Assign to QA </label>
		                        	<div class="col-sm-10">

		                        		 <select class="custom-select" id="testerid" name="testerid">
									<option value="null">- Select QA -</option>
			                            		@foreach($userroles['data'] as $userrole )
			                            	  @foreach($listuser['data'] as $listusers )
			                            		@if(in_array($userrole['user_id'],$membersexplode) && $listusers['id'] == $userrole['user_id'] && $userrole['role_id'] == 5   )
												<option value="{{ $userrole['user_id'] }}" {{ $userrole['user_id'] == $task['tester_id'] ? 'selected':'' }} >{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
												@endif
											 @endforeach
			                            	@endforeach

			                            </select>

			                           
		                            </div>
		                        </div>

		                       <input type="hidden" name="developerid" id="developerid" placeholder="title" class="form-control" value="{{ $task['developer_id'] }}">


								@else

								  <div class="form-group row">
		                        	<label class="col-form-label col-sm-2">Assign to Developer</label>
		                        	<div class="col-sm-10">

		                        		 <select class="custom-select" id="developerid" name="developerid">
									<option value="null">- Select Developer -</option>
			                            
			                            	@foreach($userroles['data'] as $userrole )
			                            	  @foreach($listuser['data'] as $listusers )
			                            		@if(in_array($userrole['user_id'],$membersexplode) && $listusers['id'] == $userrole['user_id'] && $userrole['role_id'] == 3   )
												<option value="{{ $userrole['user_id'] }}" {{ $userrole['user_id'] == $task['developer_id'] ? 'selected':'' }} >{{ $listusers['employee_name'] }} - {{ $listusers['email'] }}
												</option>
												@endif
											 @endforeach
			                            	@endforeach

			                            </select>

			                           
		                            </div>
		                        </div>


								<input type="hidden" name="testerid" id="testerid" placeholder="title" class="form-control" value="null">

								@endif 



								  <div class="form-group row">
									<label class="col-form-label col-sm-2">Start Date  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="kickoff_date" id="kickoff_date" class="form-control daterange-single"  placeholder="Please insert kickoff date" value="{{ \Carbon\Carbon::parse($task['kickoff_date'])->format('m/d/Y') }}">
									</div>

									<label class="col-form-label col-sm-2">Target End Date Dev</label>
									<div class="col-sm-5 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single"  placeholder="Please insert kickoff date" value="{{ \Carbon\Carbon::parse($task['target_end_date_dev'])->format('m/d/Y') }}">
										<input type="text" name="target_hours_dev" id="target_hours_dev" class="form-control"  placeholder="Target Hours Dev" value="{{ $task['target_hours_dev'] }}">
									</div>
								</div>


								<div class="form-group row">
									<label class="col-form-label col-sm-2">Deadline  </label>
									<div class="col-sm-3 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date" id="target_end_date" class="form-control daterange-single"  placeholder="Please insert Deadline"  value="{{ \Carbon\Carbon::parse($task['target_end_date'])->format('m/d/Y') }}">
									</div>

									<label class="col-form-label col-sm-2">Target End Date QA  </label>
									<div class="col-sm-5 input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="target_end_date_qa" id="target_end_date_qa" class="form-control daterange-single"  placeholder="Please insert kickoff date"  value="{{ \Carbon\Carbon::parse($task['target_end_date_qa'])->format('m/d/Y') }}"> 

										<input type="text" name="target_hours_qa" id="target_hours_qa" class="form-control"  placeholder="Target Hours QA" value="{{ $task['target_hours_qa'] }}">
									</div>
								</div>

								@elseif (Session::get('roleid') == 3 )

									@if($task['task_status_id'] == 1)

									<input type="hidden" name="kickoff_date_dev" id="kickoff_date_dev" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['kickoff_date_dev'])->format('m/d/Y') }}">
									<button type="submit" class="btn bg-primary">
									Submit Task</button>

									@elseif($task['task_status_id'] == 2)

									<input type="hidden" name="kickoff_date_dev" id="kickoff_date_dev" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['kickoff_date_dev'])->format('m/d/Y') }}">
								

							    	<input type="hidden" name="target_end_date_dev_real" id="target_end_date_dev_real" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['target_end_date_dev_real'])->format('m/d/Y') }}">

									 <button type="submit" class="btn bg-primary">
									 Development Done</button>

									 @else

									 @endif
									
								@else 

									@if($task['task_status_id'] == 3)

									<input type="hidden" name="kickoff_date_qa" id="kickoff_date_qa" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['kickoff_date_qa'])->format('m/d/Y') }}">
									<button type="submit" class="btn bg-primary">
									Test this Task</button>

									@elseif($task['task_status_id'] == 4)

									 <input type="hidden" name="kickoff_date_qa" id="kickoff_date_qa" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['kickoff_date_qa'])->format('m/d/Y') }}">

							    	 <input type="hidden" name="target_end_date_qa_real" id="target_end_date_qa_real" class="form-control daterange-single"  placeholder="Please insert target end date actually" value="{{ \Carbon\Carbon::parse($task['target_end_date_qa_real'])->format('m/d/Y') }}">
									 <button type="submit" class="btn bg-primary">
									 Task Done</button>

									 @else
									 
									 @endif


								@endif
								</div>


								@if(Session::get('roleid') == 1 )
								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="submit" class="btn bg-primary">Save</button>
								</div>
								@elseif (Session::get('roleid') == 3 )
								@else
								@endif
							</form>
						</div>
					</div>
				</div>
				@endforeach

{{-- <script>

	function taketaskdev(id)
	{

		 jQuery(document).ready(function(){
            jQuery('#taketask').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{action('TaskController@taketask', 38 )}}",
                  method: 'post',
                  // data: {
                  //    name: jQuery('#name').val(),
                  //    type: jQuery('#type').val(),
                  //    price: jQuery('#price').val()
                  // },
                  success: function(result){
                     jQuery('.alert').show();
                     jQuery('.alert').html(result.success);
                  }});
               });
            });
		
	}

</script>
 --}}
{{-- <script>
	function add(id)
		alert(id);

</script> --}}


{{-- <script>
	function add(id)
	{
         jQuery(document).ready(function(){
            jQuery('#taketask').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{action('TaskController@taketask', 44 )}}",
                  method: 'post',
                  // data: {
                  //    name: jQuery('#name').val(),
                  //    type: jQuery('#type').val(),
                  //    price: jQuery('#price').val()
                  // },
                  success: function(result){
                     jQuery('.alert').show();
                     jQuery('.alert').html(result.success);
                  }});
               });
            });
       }
      </script>
// function add(id)
// {
//     alert(id);
// }
</script>
 --}}

