   
   {{--  Js Default untuk Homepage --}}
    <!-- Core JS files -->
    <script src="{{ asset('global_assets/backend/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->
{{-- {{ HTML::style('css/style.css') }} --}}
    <!-- Theme JS files -->
    <script src="{{ asset('global_assets/backend/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    

    <script src="{{ asset('assets/backend/js/app.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/dashboard.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/demo_pages/form_validation.js') }}"></script>
    <script src="{{ asset('global_assets/backend/js/plugins/forms/validation/localization/messages_id.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/plugins/ui/prism.min.js') }}"></script>

    <script src="{{ asset('global_assets/backend/js/demo_pages/widgets_stats.js') }}"></script>

    


     
    
    @yield('scripts')
    @yield('js_picker_date')
    
    <!-- /theme JS files -->
    @yield('js_dashboard')

   {{--  Js untuk Datatables --}}
    @yield('js_datatables')

   {{--  Js untuk Unlock Page --}}
    @yield('js_unlock_page')


    