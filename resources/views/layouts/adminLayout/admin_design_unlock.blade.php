<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Project Management - Docotel</title>

	@include('layouts.style')
	@include('layouts.js')


</head>

<body>

	<!-- Main navbar -->
		@include('layouts.adminlayout.admin_navbar_unlock')
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">


		<!-- Main content -->
		<div class="content-wrapper">



			<!-- Content area -->
			@yield('content')
			<!-- /content area -->


			<!-- Footer -->
			@include('layouts.adminlayout.admin_footer')
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>
