<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Project Management - Docotel</title>

	@include('layouts.style')
	@include('layouts.js')
    
	<script type="text/javascript">
		$(document).ready(function() {
    var table = $('#example').DataTable({
    	
    	"stateSave": true,
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="6">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
   
} );
	</script>

	<style type="text/css">



tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

 
</head>

<body>

	<!-- Main navbar -->
		@include('layouts.adminlayout.admin_navbar')
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		@include('layouts.adminlayout.admin_sidebar')
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->

			@include('layouts.adminlayout.admin_header')
			
			<!-- /page header -->


			<!-- Content area -->
			@yield('content')
			<!-- /content area -->


			<!-- Footer -->
			@include('layouts.adminlayout.admin_footer')
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>
